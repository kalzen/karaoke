@extends('backend.layout.master')
@push('styles')
<link href="{{ asset('assets/invoice/styl.css') }}" rel="stylesheet">
@endpush
@section('content')
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li>Thống kê</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class='card mb-4'>
            <div class='card-body'>
                <div class="header">
                    <div class="card-title mb-3">Thống kê </div>
                    <form action="{!!route('admin.billdetail.statistical')!!}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="row">
                            <div class="col-md-3 form-group mb-3">
                                <label for="day">Chọn ngày</label>
                                <input type='date' name='day' class="form-control" @if(isset($day)) value='{{$day}}'@endif>
                            </div>
                            <div class="col-md-3 form-group mb-3">
                                <label for="day">Chọn tầng</label>
                                <select name="position" class="form-control">
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                </select>
                            </div>
                            <div class="text-left">
                                <button type="submit" style="margin-top:20px" class="btn btn-m btn-success">Chọn</button>
                            </div>
                            <div class="col-md-3 form-group " style="vertical-align: bottom;line-height: 71px;">
                                <button onclick="window.print(); return false;" id="print" class="btn btn-primary mb-sm-0 mb-3 print-invoice">In hóa đơn</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="print-area">
                    <div class="text-center mb30" style="margin-bottom: 30px">
                        <h1>Thống kê kho ngày {{date_format(now(), 'd/m/Y')}}</h1>
                    </div>
                    <div class="table-responsive">
                        <table  class="display table table-striped table-bordered" style="width:100%">
                            <tbody>
                                <tr>
                                    <td style="width:60px"><strong>STT</strong></td>
                                    <td class="text-left"><strong>Hàng hóa</strong></td>
                                    <td><strong>Số lượng</strong></td>
                                </tr>
                                @if(isset($each_product))
                                @foreach($each_product as $key=>$value)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td class="text-left"> {{$value->product->name}} </td>
                                    <td>{{$value->product_sum}} </td>                             
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop

