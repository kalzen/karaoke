 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quản lý karaoke</title>
    
    <link rel="icon" href="{!!asset('assets/images/favicon.ico')!!}" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cormorant+Infant:800|Noto+Serif|Roboto" rel="stylesheet">
    <link rel="stylesheet" href="{!!asset('assets/styles/vendor/custom.css')!!}">
    <link rel="stylesheet" href="{!!asset('assets/styles/css/themes/lite-purple.min.css')!!}">
    <link rel="stylesheet" href="{!!asset('assets/styles/vendor/perfect-scrollbar.css')!!}">
    <link rel="stylesheet" href="{!!asset('assets/styles/vendor/datatables.min.css')!!}">
    
    <link rel="stylesheet" href="{!!asset('assets/js/vendor/picker/picker.min.css')!!}">
    <link rel="stylesheet" href="{!!asset('assets/styles/vendor/sweetalert2.min.css')!!}">
    <link rel="stylesheet" href="{!!asset('assets/styles/vendor/bootstrap-datetimepicker.min.css')!!}">
    <link rel="stylesheet" href="{!!asset('assets/styles/vendor/jquery.datetimepicker.css')!!}">
    <link rel="stylesheet" href="{!!asset('assets/styles/css/themes/custom.css')!!}">
