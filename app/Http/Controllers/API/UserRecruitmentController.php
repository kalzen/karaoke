<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\UserRecruitmentRepository;
use Repositories\RecruitmentRepository;

class UserRecruitmentController extends Controller {

    public function __construct(UserRecruitmentRepository $userRepo, RecruitmentRepository $recruitmentRepo) {
        $this->userRepo = $userRepo;
        $this->recruitmentRepo = $recruitmentRepo;
    }

    public function apply(Request $request) {
        $input = $request->all();
        $user_id = $request->get('user_id');
        $recruitment_id = $request->get('recruitment_id');
        $apply = $this->userRepo->check($user_id, $recruitment_id);
        if ($apply->toArray() != []){
            return response()->json(['error' => false, 'log' => 'Bạn đã apply công việc này']);
        } else {
            if ($this->userRepo->create($input)) {
                return response()->json(['error' => false, 'log' => 'Ứng tuyển thành công']);
            }
        }
    }

    public function all(Request $request) {
        $user_id = $request->get('user_id');
        $id = $this->userRepo->getIds($user_id);
        $data = $this->recruitmentRepo->getUsers($id);
        return response()->json(['error' => false, 'data' => $data]);
    }
}
