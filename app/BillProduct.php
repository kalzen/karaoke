<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillProduct extends Model {

    protected $table = 'bill_product';
    protected $fillable = ['code','user_id','log','total'];
   public function user() {
        return $this->belongsTo('App\User','user_id','id');
    }
    public function created_at() {
        return date( "d/m/Y", strtotime($this->created_at));
    }
    public function updated_at() {
        return date( "d/m/Y", strtotime($this->updated_at));
    }
}
