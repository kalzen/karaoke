<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Repositories\BillRepository;
use App\Room;
use App\Bill;
use App\Product;
use App\User;

class BackendController extends Controller {

    public function __construct(BillRepository $billRepo) {
        $this->billRepo = $billRepo;
    }

    public function index() {
        $now = date_format(now(), "H:m:i");
        $room = Room::all();
        $user = User::all();
        $users1 = User::whereTime('start', '<=', $now)->whereTime('end', '>=', $now)->where('position', 1)
                ->get();
//        dd($users1);
        $users2 = User::whereTime('start', '<=', $now)->whereTime('end', '>=', $now)->where('position', 2)
                ->get();
        $room1 = $room->where('position', 1);
        $room2 = $room->where('position', 2);
        $bill = Bill::all();
        $product = Product::all();
        foreach ($room1 as $key => $value) {
            $value->yes = $value->bill()->where('status', '=', 1)->first();
            $value->no = Bill::where([['status', '=', 2], ['room_id', '=', $value->id]])->get();
        }
//        dd($room1);
        foreach ($room2 as $key => $value) {
            $value->yes = Bill::where([['status', '=', 1], ['room_id', '=', $value->id]])->first();
            $value->no = Bill::where([['status', '=', 2], ['room_id', '=', $value->id]])->get();
        }
        $this->billRepo->updateTime();
        return view('backend/index', compact('room1', 'room2', 'product', 'users1','users2'));
    }

}
