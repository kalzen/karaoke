<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;

class StockRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Stock';
    }
    
    public function findSub() {
        return $this->model->whereIn('position', \App\Stock::SUB)->get();
    }

}
