@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li><a href="{{route('admin.role.index')}}">Quản lí quyền hạn</a></li>
        <li>Cập nhật quyền hạn</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class='card mb-4'>
            <div class='card-body'>
                
                <form action="{!!route('admin.role.update',$id)!!}" method="POST" enctype="multipart/form-data">      
                     {{ csrf_field() }}
                    <div class="row mb-3 clearfix" >
                        <div class="col-sm-12">
                            <label class="required">Phân quyền <b>{!!$record->name!!}</b></label><br>
                            @foreach($routes as $route)
               
                            <label class="radio-inline col-sm-4">
                                <input name="route[]" value="{{$route}}" type="checkbox" {{in_array($route, $routed)?'checked':''}}>{{trans('route.'.$route)}}
                            </label>
                            
                            @endforeach
                        </div>
                    </div>
                    <!-- Personal Detail & Address -->
                    <div class="text-left">
                        <button type="submit" class="btn btn-m btn-success">Lưu lại</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop
@section('script')
@parent
<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker({
            pickDate: false
        });
    });
    $(function () {
        $('#datetimepicker2').datetimepicker({
            pickDate: false
        });
    });
</script> 
@stop
