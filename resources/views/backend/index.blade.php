@extends('backend.layout.master')
@section('content')
<!-- Dashboard content -->
@if (Session::has('success'))
<div class="alert bg-success alert-styled-left">
    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
    <span class="text-semibold">{{ Session::get('success') }}</span>
</div>
@endif
<div class="breadcrumb">
    <h1>Tầng 1</h1>
    <ul>
        @foreach($users1 as $value)
        @if($value->role_id !=1)
        <li><a href="#">{{$value->full_name}}</a></li>
        @endif
        @endforeach     
        <li>(Đang trực)</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row mb-4">
    @foreach($room1 as $key=>$value)
    <div class="col-md-4 mb-4">
        <div name="sub" class="card o-hidden @if(!empty($value->yes)) bg-danger @elseif(!empty($value->no->toArray())) bg-warning @else bg-success @endif text-white">
            <div class="card-header d-flex align-items-center">
                <h3 class="card-title m-0 pt-1 pb-2 text-white">
                    Phòng {{$value->name}} (@if(!empty($value->yes))Đang có khách @elseif(!empty($value->no->toArray())) Khách đã đặt @else Chưa có khách @endif)
                </h3>
            </div>
            <div class="card-body">
                @if(!empty($value->yes))
                <div class="d-flex justify-content-between mb-2">
                    <div class="flex-grow-1">
                        <p class="text-small text-white m-0 ">Tên khách:</p>
                        <p class="text-24 mb-3">{{$value->yes->name}}</p>
                    </div>
                    <div class="flex-grow-1">
                        <p class="text-small text-white m-0">Thời gian bắt đầu hát</p>
                        <p class="text-24 mb-3">{{date( "H:i:s", strtotime($value->yes->start_hour))}}</p>
                    </div>
                </div>
                <div class="d-flex justify-content-between mb-2">
                    <div class="flex-grow-1">
                        <p class="text-small text-white m-0">Ngày:</p>
                        <p class="text-24 mb-3">{{date( "d/m/Y", strtotime($value->yes->start_hour))}}</p>
                    </div>
                </div>
                @endif
                <div class="justify-content-between text-center mb-3">
                    @if(!empty($value->yes && !$value->yes->end_hour))
                    <a href="{{route('admin.bill.detail',$value->yes->id)}}" type="button" name="exp" class="btn btn-info m-1" data-id="{{$value->id}}">Order đồ</a>
                    <a href="{{route('admin.bill.editup',$value->yes->id)}}" type="button" name="exp" class="btn btn-info m-1" data-id="{{$value->id}}">Thanh toán</a>
                    <button class="btn btn-info btn-destroy-book-room" data-id="{{$value->yes->id}}">Hủy</button>
                    @endif
                     @if(!empty($value->yes->total))
                    <a href="{{route('admin.bill.invoice',$value->yes->id)}}" class="btn btn-info m-1" type="button">In hóa đơn</a>
                    @endif
                    @if(empty($value->yes))
                    <button type="button" name="send" class="btn btn-info m-1" data-toggle="modal" data-target="#expm" data-id="{{$value->id}}">Vào phòng</button>
                    @endif
                    <button type="button" name="send" class="btn btn-info m-1" data-toggle="modal" data-target="#exp" data-id="{{$value->id}}">Đặt phòng ngay</button>
                    
                </div>
                @if(!empty($value->no->toArray()))
                @foreach($value->no as $key=>$valu)
                <div class="btn-dark destroy-book-room">
                    {{$valu->user->full_name}} đặt {{date( "d/m", strtotime($valu->start_hour))}}: {{$valu->name}} đặt phòng từ lúc {{date( "H:i", strtotime($valu->start_hour))}}
                    <button class="btn btn-info btn-destroy-book-room" data-id="{{$valu->id}}">Hủy</button>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div> 
    @endforeach
</div>
<div class="breadcrumb">
    <h1>Tầng 2</h1>
    <ul>
        @foreach($users2 as $value)
         @if($value->role_id != 1)
        <li><a href="#">{{$value->full_name}}</a></li>
        @endif
        @endforeach
        <li>(Đang trực)</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row mb-4">
    @foreach($room2 as $key=>$value)
    <div class="col-md-4 mb-4">
        <div name="sub" class="card o-hidden @if(!empty($value->yes)) bg-danger @elseif(!empty($value->no->toArray())) bg-warning @else bg-success @endif text-white">
            <div class="card-header d-flex align-items-center">
                <h3 class="card-title m-0 pt-1 pb-2 text-white">
                    Phòng {{$value->name}} (@if(!empty($value->yes))Đang có khách @elseif(!empty($value->no->toArray())) Khách đã đặt @else Chưa có khách @endif)
                </h3>
            </div>
            <div class="card-body">
                @if(!empty($value->yes))
                <div class="d-flex justify-content-between mb-2">
                    <div class="flex-grow-1">
                        <p class="text-small text-white m-0 ">Tên khách:</p>
                        <p class="text-24 mb-3">{{$value->yes->name}}</p>
                    </div>
                    <div class="flex-grow-1">
                        <p class="text-small text-white m-0">Thời gian bắt đầu hát</p>
                        <p class="text-24 mb-3">{{date( "H:i:s", strtotime($value->yes->start_hour))}}</p>
                    </div>
                </div>
                <div class="d-flex justify-content-between mb-2">
                    <div class="flex-grow-1">
                        <p class="text-small text-white m-0">Ngày:</p>
                        <p class="text-24 mb-3">{{date( "d/m/Y", strtotime($value->yes->start_hour))}}</p>
                    </div>
                </div>
                @endif
                <div class="justify-content-between text-center mb-3">
                    @if(!empty($value->yes && !$value->yes->end_hour))
                    <a href="{{route('admin.bill.detail',$value->yes->id)}}" type="button" name="exp" class="btn btn-info m-1" data-id="{{$value->id}}">Order đồ</a>
                    <a href="{{route('admin.bill.editup',$value->yes->id)}}" type="button" name="exp" class="btn btn-info m-1" data-id="{{$value->id}}">Thanh toán</a>
                    <button class="btn btn-info btn-destroy-book-room" data-id="{{$value->yes->id}}">Hủy</button>
                    @endif
                    @if(empty($value->yes))
                    <button type="button" name="send" class="btn btn-info m-1" data-toggle="modal" data-target="#expm" data-id="{{$value->id}}">Vào phòng</button>
                    @endif
                    <button type="button" name="send" class="btn btn-info m-1" data-toggle="modal" data-target="#exp" data-id="{{$value->id}}">Đặt phòng ngay</button>
                    
                </div>
                @if(!empty($value->no->toArray()))
                @foreach($value->no as $key=>$valu)
                <div class="btn-dark destroy-book-room">
                    {{$valu->user->full_name}} đặt {{date( "d/m", strtotime($valu->start_hour))}}: {{$valu->name}} đặt phòng từ lúc {{date( "H:i", strtotime($valu->start_hour))}}
                    <button class="btn btn-info btn-destroy-book-room" data-id="{{$valu->id}}">Hủy</button>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div> 
    @endforeach
</div>
<!-- end of row-->
<div class="app-footer">
    <div>
        <p class="m-0">&copy; 2019 Kalzen</p>
        <p class="m-0">All rights reserved</p>
    </div>
</div>

<div class="modal fade" id="exp" tabindex="-1" role="dialog" aria-labelledby="exp" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="register_title">Đặt phòng</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{!!route('admin.bill.store')!!}" method="POST" enctype="multipart/form-data" id="frmSend">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <input type="hidden" name="status" value="2" />
                    <div class="form-group">
                        <p><strong>Người đặt: {{Auth::user()->full_name}}</strong></p>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name-2" class="col-form-label">Tên khách:</label>
                        <input type="text" class="form-control" id="recipient-name-2" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="message-text-1" class="col-form-label">Số điện thoại:</label>
                        <input type="number" class="form-control" id="message-text-1" name="phone" >
                    </div>
                    <div class="form-group">
                        <label for="message-text-1" class="col-form-label">Số người:</label>
                        <input type="number" class="form-control" id="message-text-1" name="number" required>
                    </div>
                    <div class="form-group">
                        <label for="message-text-2" class="col-form-label">Thời gian:</label>
                        <input id="datetimepicker" class="form-control" type="text" value="" name="start_hour">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" name="schedule" data-dismiss="modal">Close</button>
                        <button type="submit" id="alert-success" name="schedule"  class="btn btn-primary">Đặt phòng ngay</button>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="expm" tabindex="-1" role="dialog" aria-labelledby="expm" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="register_title">Vào phòng</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{!!route('admin.bill.store')!!}" method="POST" enctype="multipart/form-data" id="frmSen">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <input type="hidden" name="status" value="1" />
                    <div class="form-group">
                        <label for="recipient-name-2" class="col-form-label">Tên khách:</label>
                        <input type="text" class="form-control" id="recipient-name-2" name="name">
                    </div>
                    <div class="form-group">
                        <label for="message-text-1" class="col-form-label">Số người:</label>
                        <input type="number" class="form-control" id="message-text-1" name="number">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" id="alert-success"  class="btn btn-primary">Vào phòng</button>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- /dashboard content -->
@stop
@section('script')
@parent
<script>
   jQuery.datetimepicker.setLocale('vi');

jQuery('#datetimepicker').datetimepicker({
  format:'d.m.Y H:i',
 minDate:new Date(),
});

    $("button[name='send']").click(function () {
        var room_id = this.dataset.id;
        $("#frmSend").append(
                '<input value="' + room_id + '" type="hidden" name="room_id" class="delete">'
                );
        $("#frmSen").append(
                '<input value="' + room_id + '" type="hidden" name="room_id" class="delete">'
                );
    });
   

</script>
<script type="text/javascript">
    init_reload();
    function init_reload() {
        setInterval(function () {
            window.location.reload();

        }, 300000);
    }
</script>
@stop
