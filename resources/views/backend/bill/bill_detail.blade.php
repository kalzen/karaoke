@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li><a href="{{route('admin.bill.in')}}">Quản lí phiếu chi</a></li>
        <li>Chi tiết phiếu chi</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class='card mb-4'>
            <div class='card-body'>
                <div class="card-title mb-3">Chi tiết phiếu chi</div>
                <form>
                    <div class="row">
                        <label>Mã phiếu chi: {{$billpro->code}}</label>
                    </div> 
                    <div class="row">
                        <label>Người tạo: {{$billpro->user->full_name}}</label>
                    </div>
                    <div class="row">
                        <label>Ngày tạo: {{$billpro->created_at()}}</label>
                    </div>
                    <div class="row">
                        <label>Lý do chi: {{$billpro->log}}</label>
                    </div>
                    <div class="row">
                        <label>Tổng tiền: {{$billpro->total}}</label>
                    </div>
                    <!-- Personal Detail & Address -->
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop


