@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Quản lí kho</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li>Quản lí kho</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<!-- end of row -->
<div class="row mb-4">
    <h3 class="col-md-12 card-title mb-3">{!!$stock->name!!}</h3>
    <div class="col-md-12 mb-4">
        <div class="card text-left">
            <div class="card-body">
                <div class="header">
                    <h4 class="card-title mb-3">Danh sách đồ</h4>
                    <ul class="header-dropdown" style="display:flex">
                        @if($stock->position == 0)
<!--                        <li style="margin-right: 10px;"><a href="{{route('admin.stock.export',$id)}}" class="btn btn-info">Xuất kho</a></li>-->
                        <li><a href="{{route('admin.stock.import',$id)}}" class="btn btn-info">Nhập kho</a></li>
                        @endif
                    </ul>
                </div>
                <div class="table-responsive">
                    <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tên</th>
                                <th>Số lượng</th>
                                <th>Đơn vị tính</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($stock_product as $key=>$value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value->product->name}}</td>
                                <td>{{$value->number}}</td>
                                <td>{{$value->product->unit}}</td>
                            </tr>
                            @endforeach
                          
                        </tbody>
                    </table>
                </div>
                <div class="header">
                    <h4 class="card-title mb-3">Lịch sử</h4>
                </div>
                <div class="table-responsive container-fluid">
                    <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Log</th>
                                <th>Nhập từ</th>
                                <th>Đến</th>
                                <th>Hàng hóa</th>
                                <th>Ngày tạo</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($log as $key=>$value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value->log}}</td>
                                <td>{{$value->stock_from}}</td>
                                <td>{{$value->stock_to}}</td>
                                <td>
                                    @foreach($value->product_log as $v)
                                    {{$v}}<br>
                                    @endforeach
                                </td>
                                <td>{{date_format($value->created_at, 'd/m/Y')}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
<!-- Default Size -->