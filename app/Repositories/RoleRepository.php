<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Route;

class RoleRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Role';
    }

    function getAllRole() {
        $roles = $this->model->where('id', '<>', 1)->get();
        return $roles;
    }

    public function getRouteName() {
        $routes = Route::getRoutes();
        $arr = [];
        foreach ($routes as $route) {
            $route_name = $route->getName();
            if($route_name && !is_null($route_name) && strpos($route_name, 'admin')!== false){
                $arr = array_merge($arr, [$route_name]);
            }
        }
//        dd($arr);
        return $arr;
    }

}
