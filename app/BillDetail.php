<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillDetail extends Model {

    protected $table = 'bill_detail';
    protected $fillable = ['bill_id', 'product_id', 'number'];
    public $timestamps = false;
     public function product() {
        return $this->belongsTo('App\Product','product_id','id');
    }
}
