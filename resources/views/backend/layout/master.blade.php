<!DOCTYPE html>
<html lang="en">
    <head>
        @include('backend/layout/__head')
        @stack('styles')   
    </head>

   <body class="theme-cyan">
        <div class="app-admin-wrap">
            @include('backend/layout/__navbar')
            <!-- Page container -->
            <div class="main-content-wrap d-flex flex-column sidenav-open">
                <!-- Dashboard content -->
                @yield('content')   
                <!-- /dashboard content -->

                <!-- Footer -->
                @include('backend/layout/__footer')
                <!-- /content area -->
                <!-- /main content -->
            </div>
            <!-- /page content -->
        </div>
        <!-- /page container -->
    </body>
    @yield('script')   
</html>

