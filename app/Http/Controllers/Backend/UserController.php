<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Repositories\UserRepository;
use Repositories\RoleRepository;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class UserController extends Controller {

    public function __construct(
    UserRepository $userRepo, RoleRepository $roleRepo
    ) {
        $this->userRepo = $userRepo;
        $this->roleRepo = $roleRepo;
    }

    public function index(Request $request) {
        $users = $this->userRepo->getAll($request);
        $search = $request->all();
        return view('backend/user/index', compact('users', 'search'));
    }

    public function create() {
        return view('backend/user/create');
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->userRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $password = $request->get('password');
        $input['password'] = bcrypt($password);
        $this->userRepo->create($input);
        return redirect()->route('admin.user.index')->with('success', trans('base.add_success'));
    }

    public function edit($id) {
        $user = \Auth::user()->role_id;
        if ($user == 1) {
            $user = $this->userRepo->find($id);
            $roles = $this->roleRepo->all();
            return view('backend/user/update', compact('user', 'roles'));
        } else {
            abort(403);
        }
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->userRepo->validateUpdate($id));
//        if ($request->changepassword != null) {
//            if ($validator->fails()) {
//                return redirect()->back()->withErrors($validator)->withInput();
//            }
//            $password = $request->get('password');
//            $input['password'] = bcrypt($password);
//        }
        $this->userRepo->update($input, $id);
        $user = $this->userRepo->find($id);
        return redirect()->route('admin.user.index')->with('success', trans('base.update_success'));
    }

    public function destroy($id) {
        $this->userRepo->delete($id);
        return redirect()->back()->with('success', 'Xóa thành công');
    }

    public function toggle($id) {
        $this->userRepo->toggle($id);
        return redirect()->back();
    }
    public function resetdata(){
        return view('backend/user/reset');
    }
    public function reset(Request $request, $id) {
        $input = $request->all();
        $password = $request->get('password');
        $input['password'] = bcrypt($password);
        $this->userRepo->update($input, $id);
        return redirect()->route('admin.index')->with('success', trans('Cập nhật thành công'));
    }

    public function adminDel($id) {
        $this->userRepo->delete($id);
        return redirect()->back()->with('success', 'Xóa thành công');
    }

    public function toggleGroup(Request $request, $id) {

        $status = $request->get('status');
        $group = $request->get('group');
        if ($status == 2) {
            if (!is_null($group)) {
                $this->userRepo->exportUser($group, $id);
            }
            return redirect()->back();
        }
        if ($status == 3) {
            if (!is_null($group)) {
                $this->userRepo->deleteAll($group, $id);
            }
            return redirect()->back();
        }
        $this->userRepo->toggleGroup($group, $status);
        return redirect()->route('admin.user.index');
    }

}
