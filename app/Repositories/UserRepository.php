<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class UserRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\User';
    }

    public function validateCreate() {
        return $rules = [
            'password' => 'required|min:6',
            'user_name' => 'required|unique:user',
        ];
    }

    public function validateAdminCreate() {
        return $rules = [
            'password' => 'required|min:6',
            'user_name' => 'required|unique:user',
        ];
    }

    public function validateUpdate($id) {
        return $rules = [
            'password' => 'required|min:6|confirmed',
            'user_name' => 'user_name',
        ];
    }

    public function validateAdminUpdate($id) {
        return $rules = [
            'user_name' => 'user_name',
        ];
    }

    public function validateAPICreate() {
        return $rules = [
            'email' => 'email',
            'password' => 'required|min:6'
        ];
    }

//    function getAllUser() {
//        $users = $this->model->where('role_id', '<>', \App\User::ROLE_ADMIN)->get();
//        return $users;
//    }
    public function deleteAll($group, $id) {
        if (in_array(0, $group)) {
            $user = $this->model->whereNotIn('id', $group)->where('role_id', $id)->delete();
        } else {
            foreach ($group as $key => $val) {
                $query = $this->model->where('id', '=', $val)->delete();
            }
        }
        return true;
    }

    public function getAdmin() {
        $query = $this->model->whereIn('role_id', \App\Role::ROLE_QTV)->get();
        return $query;
    }

    function getAll($request) {
        $query = $this->model;

        if (!is_null($request->get('orderBy'))) {
            $query = $query->orderBy('lastname', $request->get('orderBy'));
        }

        if (!is_null($request->get('keyword'))) {
            $query = $query->where('firstname', 'LIKE', '%' . $request->get('keyword') . '%')->orWhere('lastname', 'LIKE', '%' . $request->get('keyword') . '%');
        }

        if (!is_null($request->get('bussiness_id'))) {
            $query = $query->where('bussiness_id', $request->get('bussiness_id'));
        }
        $user = $query->orderBy('id', 'ASC')->get();
        return $user;
    }

    public function toggleGroup($group, $status) {
        if (in_array(0, $group)) {
            $user = $this->model;
        } else {
            $check = true;
            if ($group[0] < 0) {
                $check = false;
                foreach ($group as $key => $value) {
                    $group[$key] = abs($value);
                }
            }
            if ($check == false) {
                $user = $this->model->whereNotIn('id', $group);
            } else {
                $user = $this->model->whereIn('id', $group);
            }
        }
        $user->where('status', '<>', $status)->update(['status' => $status]);
    }

    public function exportUser($group, $id) {
        if (in_array(0, $group)) {
            $users = $this->model->where('role_id', $id)->get();
        } else {
            $check = true;
            if ($group[0] < 0) {
                $check = false;
                foreach ($group as $key => $value) {
                    $group[$key] = abs($value);
                }
            }
            if ($check == false) {
                $users = $this->model->whereNotIn('id', $group)->get();
            } else {
                $users = $this->model->whereIn('id', $group)->get();
            }
        }
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A1', trans('base.id'))
                ->setCellValue('B1', trans('base.fullname'))
                ->setCellValue('C1', trans('base.tel'))
                ->setCellValue('D1', trans('base.email'))
                ->setCellValue('E1', trans('Ngành nghề'))
                ->setCellValue('F1', trans('Địa chỉ'))
                ->setCellValue('G1', trans('base.created_at'));
        foreach ($users as $key => $user) {
            $row = $key + 2;
            $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A' . $row, $user->id)
                    ->setCellValue('B' . $row, $user->firstname . $user->lastname)
                    ->setCellValue('C' . $row, $user->phone)
                    ->setCellValue('D' . $row, $user->email)
                    ->setCellValue('E' . $row, $user->bussiness->name)
                    ->setCellValue('F' . $row, $user->region->name)
                    ->setCellValue('G' . $row, $user->created_at());
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="danh-sach-thanh-vien.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }
    public function count($role_id){
        $user=$this->model->where('role_id',$role_id)->get();
        $user_count=count($user);
        return $user_count;
    }
     public function getUsers($data)
    {
        return $this->model->whereIn('id',$data)->paginate(10);
    }
}
