<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockProduct extends Model {

    protected $table = 'stock_product';
    protected $fillable = ['stock_id','product_id','number'];
     public function product() {
        return $this->belongsTo('App\Product','product_id','id');
    }
   public $timestamps = false;
}
