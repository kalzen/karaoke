@extends('backend.layout.master')
@section('content')
@push('styles')
<link href="{{ asset('assets/invoice/sty.css') }}" rel="stylesheet">
@endpush
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li><a href="{{route('admin.bill.index')}}">Quản lí hóa đơn</a></li>
        <li>Tạo hóa đơn</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card-body">
            <div class="container">
                <!-- Block 1 -->
                <div id="print-area">
                    <h2>Danh mục món ăn</h2>
                    <form id='frmorder'>
                        <div class="table-responsive">
                            <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên món</th>
                                        <th>Số lượng</th>
                                        <th>Đơn vị tính</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($product as $key=>$value)
                                    <tr class="no-print">
                                        <td class="col-id">{{$key+1}}</td>
                                        <td class="col-name">{{$value->name}}</td>
                                        <td class="col-price">
                                            <input type="text" class="put" name="{{$value->id}}" value="0" data-max="{{$value->max}}">
                                             <div class="err-max"></div>
                                        </td>
                                        <td>{{$value->unit}}</td>
                                    </tr>
                                    @endforeach
                                    @foreach($products as $key=>$value)
                                    <tr class="no-print">
                                        <td class="col-id">{{$key+1}}</td>
                                        <td class="col-name">{{$value->name}}</td>
                                        <td class="col-price">
                                            <input type="text" class="put" name="{{$value->id}}" value="0" data-max="{{$value->max}}">
                                            <div class="err-max"></div>
                                        </td>
                                        <td>{{$value->unit}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <input type="hidden" name="bill_id" id="bill" value="{{$bill->id}}">
                    </form>
                </div>
                <button type="button" id='order' class="btn btn-success m-1"> Order đồ </button>
                <button onclick="window.print(); return false;" class="btn btn-primary mb-sm-0 mb-3 print-invoice">In đồ</button>
                <!-- Block 2 -->

            </div>
        </div> 
    </div>
</div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop
@section('script')
@parent
<script>
    $(".col-price").append('<button type="button" style="width: 40px;height: 30px;margin-bottom: 3px;" data-id=1 class="but btn btn-success"> <i class="nav-icon i-Up"></i></button>\n\
                         <button type="button" style="width: 40px;height: 30px;margin-bottom: 3px;" data-id=2 class="but btn btn-danger"> <i class="nav-icon i-Down"></i></button>');
    var table = $('#zero_configuration_table').DataTable();
    $("#order").click(function () {
        var data = table.$('input').serialize();
        var bill_id = $('#bill').val();
        $.ajax({

            url: "/api/createOrder",
            method: "POST",
            data: {data:data,
                    _token: '{!!csrf_token()!!}',
                    bill_id:bill_id
            },
            success: function (response) {
                if (response.sucess == true) {
                    location.href = '/';
                }
            }
        });
    });
</script>
@stop