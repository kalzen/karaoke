<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cv;
use Repositories\CvRepository;
use Repositories\ShipCategoryRepository;
use Repositories\PositionCategoryRepository;

class CvController extends Controller {

    public function __construct(CvRepository $cvRepo, ShipCategoryRepository $shipRepo, PositionCategoryRepository $positionRepo) {
        $this->cvRepo = $cvRepo;
        $this->shipRepo = $shipRepo;
        $this->positionRepo = $positionRepo;
    }

    public function index(Request $request) {
        $cvs = $this->cvRepo->getAll($request);
        return view('backend/cv/index', compact('cvs'));
    }

    public function create() {
        $ships = $this->shipRepo->all();
        $positions = $this->positionRepo->all();
        return view('backend/cv/create', compact('ships', 'positions'));
    }

    public function store(Request $request) {
        $input = $request->all();
        $this->cvRepo->create($input);
        return redirect()->route('admin.cv.index')->with('success', trans('base.add_success'));
    }

    public function edit($id) {
        $cv = $this->cvRepo->find($id);
        $ships = $this->shipRepo->all();
        $positions = $this->positionRepo->all();
        return view('backend/cv/update', compact('cv','ships','positions'));
    }

    public function update(Request $request, $id) {
        $cv = $this->cvRepo->find($id);
        $input = $request->all();
        $this->cvRepo->update($input, $id);
        return redirect()->back()->with('success', trans('base.update_success'));
    }

    public function destroy($id) {
        $this->cvRepo->delete($id);
        return redirect()->back()->with('success', trans('base.delete_success'));
    }

}
