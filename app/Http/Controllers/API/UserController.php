<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\UserRepository;
use Repositories\RoleRepository;
use Mail;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller {

    public function __construct(UserRepository $userRepo, RoleRepository $roleRepo) {
        $this->userRepo = $userRepo;
        $this->roleRepo = $roleRepo;
    }

    public function listAll() {
        $users = User::all();
        return response()->json($users);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function signup(Request $request, JWTAuth $JWTAuth) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->userRepo->validateAPICreate());

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return response()->json([
                        'error' => 'true',
                        'log' => $error
            ]);
        }
        $status = User::where('email', $input['email'])->first();
        if ($status) {
            return response()->json([
                        'error' => 'true',
                        'log' => 'Tài khoản đã tồn tại'
            ]);
        }
        $password = $request->get('password');
        $input['password'] = bcrypt($password);
        $user = $this->userRepo->create($input);
        $credentials = ['email'=>$user->email, 'password'=>$password];
        try {
            $token = $JWTAuth->attempt($credentials);
        } catch (JWTException $e) {
            throw new HttpException(500);
        }

        return response()->json([
                    'error' => 'false',
                    'token' => $token,
                    'user' => $user
        ]);
    }

    public function updateUser($user_id, Request $request) {
        $input['phone_number'] = $request->get('phone_number');
        $input['birthday'] = date('Y-m-d', strtotime($request->get('birthday')));
        $input['gender'] = $request->get('gender');
        $input['password'] = bcrypt($request->get('password'));

        if ($this->userRepo->update($input, $user_id)) {

            return response()->json(['error' => false, 'log' => 'Cập nhật thành công!']);
        }
    }

    public function updatePhone($user_id, $phone_number) {
        $input['phone_number'] = $phone_number;
        if ($this->userRepo->update($input, $user_id)) {
            return response()->json(['error' => false, 'log' => 'Cập nhật thành công!']);
        }
    }

    public function resetpassword(Request $request) {
        $email = $request->get('email');
        $user = $this->userRepo->whereBy('email', $email)->first();
        $pass = str_random(10);
        $input['password'] = bcrypt($pass);
        $this->userRepo->update($input, $user->id);
        Mail::send('mailfb', array('pass' => $pass), function($message) use ($email) {
            $message->to($email, 'Visitor')->subject('Mật khẩu mới của bạn là:');
        });
        return response()->json([
                    'log' => 'Hãy kiểm tra email của bạn để lấy mật khẩu mới nhất',
        ]);
    }

    public function getInfo($id) {
        $user = $this->userRepo->find($id);
        return response()->json([
                    'error' => 'false',
                    'data' => $user
        ]);
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        if ($this->userRepo->update($input, $id)) {
            return response()->json(['error' => false, 'log' => 'Cập nhật thành công!']);
        }
    }

}
