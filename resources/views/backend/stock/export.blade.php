@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li><a href="{{route('admin.stock.index',$id)}}">Quản lí kho</a></li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card-body">
            <div class="container">
                <!-- Block 1 -->
                <h2>Danh sách đồ</h2>
                <form id='frminout'>
                    <div class="table-responsive">
                        <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên đồ</th>
                                    <th>Số lượng</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($product as $key=>$value)
                                <tr>
                                    <td class="col-id">{{$key+1}}</td>
                                    <td class="col-name">{{$value->name}}</td>
                                    <td class="col-price">
                                        <input type="text" class="put" name="{{$value->id}}" data-max="{{$value->max}}" value="0">
                                        <span class="err-max text-danger"></span>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <label>Thông báo:</label>
                    <textarea name="log" class="form-control" ></textarea>
                    @if($id==1)
                    <br>
                    <label>Xuất đến kho:</label>
                    <select name="stock_from" class="form-control">
                        @foreach($sub as $value)
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endforeach
                    </select>
                    @endif
                    <br>
                    <input type="hidden" name="stock_id" value="{{$id}}">
                </form>
                <button type="button" id='out' class="btn btn-success m-1"> Xuất kho</button>
                <!-- Block 2 -->
            </div>
        </div>                          
    </div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop
@section('script')
@parent
<script>
    $(".col-price").append('<button type="button" style="width: 40px;height: 30px;margin-bottom: 3px;" data-id=1 class="but btn btn-success"> <i class="nav-icon i-Up"></i></button>\n\
                         <button type="button" style="width: 40px;height: 30px;margin-bottom: 3px;" data-id=2 class="but btn btn-danger"> <i class="nav-icon i-Down"></i></button>');
    $("#in").click(function () {
        $.ajax({
            url: "/api/inputStock",
            method: "POST",
            data: $('#frminout').serialize(),
            success: function (response) {
                location.href = '/stock/' + response.id;
            }
        });
    });
    $("#out").click(function () {
        $.ajax({
            url: "/api/outStock",
            method: "POST",
            data: $('#frminout').serialize(),
            success: function (response) {
                location.href = '/stock/' + response.id;
            }
        });
    });

</script>
@stop