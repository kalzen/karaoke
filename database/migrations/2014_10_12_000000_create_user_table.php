<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name')->nullable();
            $table->string('password')->nullable();
            $table->string('user_name')->nullable();
            $table->string('phone')->nullable();
            $table->date('start_day')->nullable();
            $table->time('start')->nullable();
            $table->time('end')->nullable();
            $table->integer('remember')->nullable();         
            $table->integer('role_id')->nullable();     
            $table->rememberToken();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('user');
    }

}
