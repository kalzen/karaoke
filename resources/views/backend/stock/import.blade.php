@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li><a href="{{route('admin.stock.index',$id)}}">Quản lí kho</a></li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card-body">
            <div class="container">
                <!-- Block 1 -->
                <h2>Danh sách đồ</h2>
                <form id='frminout'>
                       {{csrf_field()}}
                    <div class="table-responsive">
                        <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên đồ</th>
                                    <th>Số lượng</th>
                                    <th>Đơn vị tính</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($product as $key=>$value)
                                <tr>
                                    <td class="col-id">{{++$key}}</td>
                                    <td class="col-name">{{$value->name}}</td>
                                    <td class="col-price"><input type="text" class="put" name="{{$value->id}}" value="0"></td>
                                    <td class="col-unit">{{$value->unit}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <label>Thông báo:</label>
                    <textarea name="log" class="form-control"></textarea>
                    <input type="hidden" name="stock_id" id="stock" value="{{$id}}">
                    <br>
                </form>
                <button type="button" id='in' class="btn btn-success m-1"> Nhập kho</button>
                <!-- Block 2 -->
            </div>
        </div>                          
    </div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop
@section('script')
@parent
<script> 
    $(".col-price").append('<button type="button" style="width: 40px;height: 30px;margin-bottom: 3px;" data-id=1 class="but btn btn-success"> <i class="nav-icon i-Up"></i></button>\n\
                         <button type="button" style="width: 40px;height: 30px;margin-bottom: 3px;" data-id=2 class="but btn btn-danger"> <i class="nav-icon i-Down"></i></button>');
    var table = $('#zero_configuration_table').DataTable();
    $("#in").click(function () {
    var data = table.$('input').serialize();
    var stock_id=$('#stock').val();
    $.ajax({
    url: "/inputStock",
            method: "POST",
            data: {data:data,
                    _token: '{!!csrf_token()!!}',
                    stock_id:stock_id
            },
            success: function (response) {
            location.href = '/stock/' + response.id;
            }
    });
    });
    $("#out").click(function () {
    $.ajax({
    url: "/api/outStock",
            method: "POST",
            data: $('#frminout').serialize(),
            success: function (response) {
            location.href = '/stock/' + response.id;
            }
    });
    });
    $(".stock_log").click(function () {
    var stock_to = this.dataset.id;
    $.ajax({
    url: "/api/outStock/" + stock_to,
            method: "POST",
            data: $('#frminout').serialize(),
            success: function (response) {
            location.href = '/stock/' + response.id;
            }
    });
    });
</script>
@stop