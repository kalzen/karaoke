@extends('backend.layout.master')
@push('styles')
<link href="{{ asset('assets/invoice/style.css') }}" rel="stylesheet">
@endpush
@section('content')
<div class="breadcrumb">
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li><a href="{{route('admin.bill.index')}}">Quản lí hóa đơn</a></li>
        <li>Tạo hóa đơn</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="card">
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade active show" role="tabpanel" aria-labelledby="invoice-tab">
            <div class="d-sm-flex mb-5" data-view="print">
                <span class="m-auto"></span>
                @if($bill->status == 1)
                <button onclick="window.print(); return false;" id="print" data-id="{{$bill->id}}" class="btn btn-primary mb-sm-0 mb-3 print-invoice">In hóa đơn</button>
                @endif

            </div>
            <!---===== Print Area =======-->
            <div id="print-area">
                <header class="clearfix row">
                    <div id="logo" class="col-md-4">
                        <img src="{{asset('assets/invoice/logo-imperial.jpg')}}">
                    </div>
                    <div class="col-md-8" style="margin-top: 20px;">
                        <h1 class="name" style="font-family: Times New Roman, Times, serif;font-size: 31pt;"><strong>IMPERIA KARAOKE</strong></h1>
                        <div style="font-family: Times New Roman, Times, serif;"><span style="font-size: 19pt;font-weight:300!important;">Số 8 Lô 3C Lê Hồng Phong - Ngô Quyền - Hải Phòng</span></div>
                        <div class="name" style="font-size: 23pt;"><strong>Tell:  0225.38.26899-Hotline:0846.055.599</strong></div>
                    </div>

                </header>
                <main>
                    <div id="details" class="clearfix">
                        <div>
                            <h1 class="name" style="text-align: center;font-size: 32pt;font-family: Times New Roman, Times, serif;"><strong>HÓA ĐƠN THANH TOÁN</strong></h1>
                        </div>
                        <div id="client" class="row">
                            <p class="col-md-4"><span class="name" style="font-size:21pt;">No: </span><span>{{$bill->code}}</span></p>
                            <p class="col-md-2"><span class="name" style="font-size:21pt;">P: {{$bill->room->name}}</span></p>
                            <p class="col-md-3"><span class="name" style="font-size:21pt;">Ngày:  </span><i>{{$bill->created_at()}}</i></p>
                            <p class="col-md-3" style="text-align: left;font-size:21pt;"><span class="name">Giờ vào: {{date( "H:i:s", strtotime($bill->start_hour))}}</span></p>
                        </div>
                        <div id="client" class="row">
                            <p class="col-md-9"><span class="name" style="font-size:21pt;">Khách hàng: </span><span>Khách lẻ</span></p>
                            <p class="col-md-3" style="text-align: left;font-size:21pt;"><span class="name">Giờ ra &nbsp:    {{date( "H:i:s", strtotime($bill->end_hour))}}</span></p>
                        </div>
                    </div>
                    <table id="bill-invoice" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr class="border">
                                <td class="nam"><b>STT</b></td>
                                <td class="desc nam"><b>DỊCH VỤ</b></td>
                                <td class="nam"><b>ĐVT</b></td>
                                <td class="unit nam"><b>SỐ LƯỢNG</b></td>
                                <td class="qty nam"><b>ĐƠN GIÁ</b></td>
                                <td class="total nam"><b>THÀNH TIỀN</b></td>
                            </tr>
                            <tr class="border">
                                <td class="">1</td>
                                <td class="desc">Giờ hát : {{round(((strtotime(date($bill->end_hour))- strtotime(date($bill->start_hour)))/3600),1)}}</td>
                                <td class="">giờ</td>
                                <td class="qty">{{round(((strtotime(date($bill->end_hour))- strtotime(date($bill->start_hour)))/3600),1)}}</td>
                                <td class="unit">{{number_format($bill->room->price)}}</td>
                                <td class="total" id='hour'>{{number_format(round(((strtotime(date($bill->end_hour))- strtotime(date($bill->start_hour)))/3600),1) * $bill->room->price)}}</td>
                            </tr>
                            @foreach($bill->billdetail as $key=>$value) 
                            <tr class="border">
                                <td class="" >{{$key+2}}</td>
                                <td class="desc">{{$value->product->name}}</td>
                                <td>{{$value->product->unit}}</td>
                                <td class="qty">{{$value->number}}</td>
                                <td class="unit">{{number_format($value->product->price)}}</td>
                                <td class="total amount">{{number_format($value->number * $value->product->price)}}</td>
                            </tr>
                            @endforeach
                            <tr class="border">
                                <td ></td>
                                <td class="desc">Phụ phí</td>
                                <td ></td>
                                <td ></td>
                                <td class="qty"></td>
                                <td class="total">@if($bill->status == 1)<input class='form-control money' id="surcharge" type='text'> @else{{number_format($bill->surcharge)}} @endif</td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="3" style="font-family: Times New Roman, Times, serif;">Tổng tiền hóa đơn:</td>
                                <td>{{number_format($bill->total)}}</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="name"  style="font-family: Times New Roman, Times, serif;">Thu ngân</td>
                                <td colspan="3" style="font-family: Times New Roman, Times, serif;">Giảm giá giờ hát:</td>
                                <td style='vertical-align: middle;line-height: 20px;'>
                                    @if($bill->status == 1)
                                    <div class="row">
                                        <input class='form-control col-md-3 offset-7' id="percentage1" type='text'>
                                        <div class="input-group-append col-md-2">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                    @else
                                    {{$bill->sale}}
                                    @endif
                                </td>
                            </tr>                                                 
                                <td class="border-none" colspan="5" style="font-family: Times New Roman, Times, serif;">Giảm giá hóa đơn:</td>
                                <td style='vertical-align: middle;line-height: 20px;'>
                                    @if($bill->status == 1)
                                    <div class="row">
                                        <input class='form-control offset-7 col-md-3' id="billdetail" type='text'>
                                        <div class="input-group-append col-md-2">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                    @else
                                    {{$bill->sale_bill}}
                                    @endif
                                </td>
                            
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="3" class="name" style="font-weight:bold">Phải thanh toán:</td>
                                <td><strong><span id='amount_total' class="name">{{number_format($bill->total)}}</span></strong></td>
                            </tr>
                            <tr>
                                <td colspan="6"><textarea class='form-control fonted'></textarea></td>
                            </tr>
                        </tbody>
                        <input type='hidden' class='singing_time' value="{{number_format(round(((strtotime(date($bill->end_hour))- strtotime(date($bill->start_hour)))/3600),1) * $bill->room->price)}}">

                    </table>
                </main>
                <footer>
                    <span style="font-family: Times New Roman, Times, serif;font-style:italic;font-size:26pt;"><b>Chân thành cảm ơn Quý khách !</b></span>
                </footer>
            </div>
            <!--==== / Print Area =====-->
        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script>
    $("#print").click(function () {
        var id = this.dataset.id;
        var total = $('#amount_total').html().replace(/,/g, '');
        var surcharge = $('#surcharge').val().replace(/,/g, '');
        var sale = $('#percentage1').val().replace(/,/g, '');
        var sale_bill = $('#billdetail').val().replace(/,/g, '');
        $.ajax({
            url: "/api/confirm",
            method: "POST",
            data: {id: id, total: total, surcharge: surcharge, sale: sale, sale_bill: sale_bill},
            success: function (response) {
                if (response.sucess == true) {
                    location.href = '/bill';
                }
            }
        });
    });
    $('input').on('keyup', function () {
        calculate();
        total();
    });
    var total = function () {
        var sum = 0;
        $('.amount').each(function () {
            var num = $(this).html().replace(/,/g, '');
            sum += parseFloat(num);
        })
        sum += parseFloat($('#hour').html().replace(/,/g, ''));
        sum = sum * (100 - Number($('#billdetail').val())) / 100;
        sum += Number($('#surcharge').val().replace(/,/g, ''));
        sum = numeral(sum).format('0,0');
        $('#amount_total').html(sum);
    }
    $('select').change(function () {
        calculate();
        total();
    });

    function calculate() {
        var price1 = $('.singing_time').val().replace(/,/g, '');
        var percentage1 = (100 - Number($('#percentage1').val())) / 100;
        $('#hour').html(price1 * percentage1);
    }
</script>
@stop