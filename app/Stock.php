<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model {
    const MAIN = 0;
    const SUB = [1,2];
    
    protected $table = 'stock';
    protected $fillable = ['name'];
    
    public function products(){
    	return $this->belongsToMany('App\Product', 'stock_product', 'stock_id', 'product_id');
    }
    
}
