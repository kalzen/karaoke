<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('role')->insert([
            'id' => '1',
            'name' => 'admin',
            'route' => 'all'
        ]);
        DB::table('role')->insert([
            'id' => '2',
            'name' => 'Quản lí',
            'route' => 'all'
        ]);
        DB::table('role')->insert([
            'id' => '3',
            'name' => 'Đặt phòng',
            'route' => 'all'
        ]);
        DB::table('role')->insert([
            'id' => '4',
            'name' => 'Order đồ',
            'route' => 'all'
        ]);
        
        
    }

}
