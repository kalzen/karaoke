@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li><a href="{{route('admin.user.index')}}">Quản lí nhân viên</a></li>
        <li>Thêm nhân viên</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class='card mb-4'>
            <div class='card-body'>
                <div class="card-title mb-3">Thêm nhân viên</div>
                <form action="{!!route('admin.user.store')!!}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="row  mb-3 clearfix">
                        <div class="col-sm-4">
                            <label>Họ tên</label>
                            <input type="text" class="form-control" name="full_name">
                        </div> 
                        <div class="col-sm-4">
                            <label>Username</label>
                            <input type="text" class="form-control" name="user_name">
                        </div> 
                        <div class="col-sm-4">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>
                    <div class="row mb-3 clearfix">
                        <div class="col-sm-4  row">
                            <div class="col-sm-5">
                                <label>Làm từ</label>        
                                <input class="form-control" id="datetimepicker3" type="text" name="start">  
                            </div>
                            <div class="col-sm-5">
                                <label>Đến</label>   
                                <input class="form-control" id="datetimepicker4" type="text" name="end">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>Tầng</label>
                            <input type="number" class="form-control" name="position">
                        </div>
                        <div class="col-sm-3">
                            <label>Ngày bắt đầu làm</label>
                            <input type="date"   class="form-control" name="start_day">
                        </div>
                        <div class="col-sm-3">
                            <label>Số điện thoại</label>
                            <input type="text" class="form-control" name="phone">
                        </div>
                    </div>
                    <div class="row mb-3 clearfix" >
                        <div class="col-sm-12">
                            <label class="required">{{trans('base.role')}}</label><br>
                            <label class="radio-inline" style="margin-right: 10px;">
                                <input name="role_id" value="2" type="radio">Quản lí
                            </label>  
                            <label class="radio-inline" style="margin-right: 10px;">
                                <input name="role_id" value="3" type="radio">Kế toán
                            </label> 
                            <label class="radio-inline" style="margin-right: 10px;">
                                <input name="role_id" value="4" type="radio">Kho
                            </label>
                            <label class="radio-inline">
                                <input name="role_id" value="5" type="radio">Nhân viên
                            </label>
                        </div>
                    </div>
                    <!-- Personal Detail & Address -->
                    <div class="text-left">
                        <button type="submit" class="btn btn-m btn-success">Lưu lại</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop
@section('script')
@parent
<script type="text/javascript">
    $(function () {
        $('#datetimepicker3').datetimepicker({
            datepicker: false,
            format: 'H:i'
        });
    });
    $(function () {
        $('#datetimepicker4').datetimepicker({
            datepicker: false,
            format: 'H:i'
        });
    });
</script> 
@stop
