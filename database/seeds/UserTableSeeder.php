<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('user')->insert([
            'full_name' => 'Administrator',
            'user_name'=>'admin',
            'password' => bcrypt('123456'),
            'role_id' => 1,
            'phone' => '09694980911',
        ]);
        DB::table('user')->insert([
            'full_name' => 'Nguyễn Văn Nam',
            'user_name'=>'nhanvien1',
            'password' => bcrypt('123456'),
            'role_id' => 2,
            'phone' => '09694980911',
        ]);
        DB::table('user')->insert([
            'full_name' => 'Nguyễn Văn Linh',
            'user_name'=>'nhanvien2',
            'password' => bcrypt('123456'),
            'role_id' => 3,
            'phone' => '09694980911',
        ]);
        DB::table('user')->insert([
            'full_name' => 'Nguyễn Văn Bình',
            'user_name'=>'nhanvien3',
            'password' => bcrypt('123456'),
            'role_id' => 4,
            'phone' => '09694980911',
        ]);

        // Create 50 product records
    }

}
