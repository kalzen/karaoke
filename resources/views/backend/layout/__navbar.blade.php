<div class="main-header">
    <div class="logo">
        <a href="{!!route('admin.index')!!}">
            <img src="{!!asset('assets/images/logo-imperial.png')!!}" alt="">
        </a>

    </div>

    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>


    <div style="margin: auto"></div>
    <div class="clock">
        <!--<div id="Date"></div>-->
        <ul>
            <li id="hours"></li>
            <li id="point">:</li>
            <li id="min"></li>
            <li id="point">:</li>
            <li id="sec"></li>
        </ul>
    </div>
    <div class="header-part-right">
        <!-- Full screen toggle -->
        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
        <!-- Grid menu Dropdown -->
        <!-- Clock -->

        <!-- Clock End -->

        <!-- User avatar dropdown -->
        <div class="dropdown">
            <div class="user colalign-self-end">
                <img src="{!!asset('assets/images/faces/1.jpg')!!}" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i> {{Auth::user()->full_name}}
                    </div>
                    <a class="dropdown-item" href="{{route('admin.user.resetdata',Auth::user()->id)}}">Thay mật khẩu</a>
                    <a class="dropdown-item" href="{{route('logout')}}">Đăng xuất</a>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="side-content-wrap">
    <div class="sidebar-left open" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">
            @if(Auth::user()->role_id == 1)
            <li class="nav-item" >
                <a class="nav-item-hold" href="{{route('admin.role.index')}}">
                    <i class="nav-icon i-Checked-User"></i>
                    <span class="nav-text">Phân quyền</span>
                </a>
                <div class="triangle"></div>
            </li>
            @endif
            @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2 )
            <li class="nav-item" >
                <a class="nav-item-hold" href="{{route('admin.user.index')}}">
                    <i class="nav-icon i-Checked-User"></i>
                    <span class="nav-text">Quản lý nhân viên</span>
                </a>
                <div class="triangle"></div>
            </li>
            @endif
            @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2 )
            <li class="nav-item" >
                <a class="nav-item-hold" href="{{route('admin.room.index')}}">
                    <i class="nav-icon i-Safe-Box"></i>
                    <span class="nav-text">Quản lý phòng</span>
                </a>
                <div class="triangle"></div>
            </li>
            @endif
            @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2 )
            <li class="nav-item" >
                <a class="nav-item-hold" href="{{route('admin.product.index')}}">
                    <i class="nav-icon i-ID-Card"></i>
                    <span class="nav-text">Quản lý menu</span>
                </a>
                <div class="triangle"></div>
            </li>
            @endif
            @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2 || Auth::user()->role_id == 4)
            <li class="nav-item" data-item="stock">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-File-Horizontal"></i>
                    <span class="nav-text">Quản lí kho</span>
                </a>
                <div class="triangle"></div>
            </li>
            @endif
            @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2 || Auth::user()->role_id == 3)
            <li class="nav-item" >
                <a class="nav-item-hold" href="{{route('admin.bill.index')}}">
                    <i class="nav-icon i-Billing"></i>
                    <span class="nav-text">Quản lý hóa đơn</span>
                </a>
                <div class="triangle"></div>
            </li>
            @endif
            @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2 || Auth::user()->role_id == 3)
            <li class="nav-item" >
                <a class="nav-item-hold" href="{{route('admin.bill.in')}}">
                    <i class="nav-icon i-Billing"></i>
                    <span class="nav-text">Quản lý doanh thu</span>
                </a>
                <div class="triangle"></div>
            </li>
            @endif
        </ul>
    </div>
    <div class="sidebar-left-secondary" data-perfect-scrollbar data-suppress-scroll-x="true">
        <!-- Submenu Dashboards -->
        <ul class="childNav" data-parent="stock">
<!--            @foreach($warehouse as $value)
            <li class="nav-item">
                <a href="{{route('admin.stock.index',$value->id)}}">
                    <i class="nav-icon i-File-Horizontal"></i>
                    <span class="item-name">{{$value->name}}</span>
                </a>
            </li>
            @endforeach-->
             <li class="nav-item">
                <a href="{{route('admin.stock.index',1)}}">
                    <i class="nav-icon i-File-Horizontal"></i>
                    <span class="item-name">Kho tổng</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.billdetail.statis')}}">
                    <i class="nav-icon i-File-Horizontal"></i>
                    <span class="item-name">Thống kê kho</span>
                </a>
            </li>
        </ul>
        <div class="sidebar-overlay"></div>
    </div>