<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bussiness extends Model
{
    protected $table='bussiness';
    protected $fillable=['name','ordering','status'];
    public function created_at() {
        return date( "d/m/Y", strtotime($this->created_at));
    }
    public function updated_at() {
        return date( "d/m/Y", strtotime($this->updated_at));
    }
}
