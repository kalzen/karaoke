<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\RecruitmentRepository;
use Repositories\UserRecruitmentRepository;
use Repositories\UserRepository;

class RecruitmentController extends Controller {

    public function __construct(RecruitmentRepository $recruitmentRepo, UserRecruitmentRepository $userRepo, UserRepository $usersRepo) {
        $this->recruitmentRepo = $recruitmentRepo;
        $this->userRepo = $userRepo;
        $this->usersRepo = $usersRepo;
    }

    public function index(Request $request) {
        if ($data = $this->recruitmentRepo->getAllRecruitment($request))
        {
            return response()->json(['error' => false, 'data' => $data]);
        } else {
            return response()->json(['error' => true, 'log' => 'Dữ liệu không tồn tại']);
        }
    }

    public function getAll() {
        $data = $this->recruitmentRepo->all();
        if ($data->toArray() != []) {
            return response()->json(['error' => false, 'data' => $data]);
        } else {
            return response()->json(['error' => true, 'log' => 'Dữ liệu không tồn tại']);
        }
    }

    public function create(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->recruitmentRepo->validateAPICreate());

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return response()->json([
                        'error' => 'true',
                        'log' => $error
            ]);
        }
        if ($this->recruitmentRepo->create($input)) {
            return response()->json(['error' => false, 'log' => 'Thêm tin thành công']);
        }
    }

    public function edit($id) {    
       if( $data = $this->recruitmentRepo->find($id)){
        return response()->json(['error' => false, 'data' => $data]);
       }else{
           return response()->json(['error'=>true,'log'=>'Dữ liệu không tồn tại']);
       }
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        if ($this->recruitmentRepo->update($input, $id)) {
            return response()->json(['error' => false, 'log' => 'Cập nhật thành công']);
        }
    }

    public function allUser(Request $request) {
        $user_id = $request->get('user_id');
        $data = $this->recruitmentRepo->whereBy('user_id', $user_id);
        return response()->json(['error' => false, 'data' => $data]);
    }

    public function getUser(Request $request) {
        $user_id = $this->userRepo->getIdUser($request->get('recruitment_id'));
        if($data = $this->usersRepo->getUsers($user_id)){
        return response()->json(['error' => false, 'data' => $data]);
        }
        else{
            return response()->json(['error' => true,'log' => 'Dữ liệu không tồn tại']);
        }
    }

}
