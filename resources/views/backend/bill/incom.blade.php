@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li>Doanh thu</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class='card mb-4'>
            <div class='card-body'>
                <div class="header">
                    <h4 class="card-title mb-3">Danh sách phiếu chi</h4>
                    <ul class="header-dropdown">
                    <li><a href="{{route('admin.billpro.createin')}}" class="btn btn-info">Thêm</a></li>
                </ul>
                </div>
                <div class="table-responsive">
                    <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Mã hóa đơn</th>
                                <th>Người tạo</th>
                                <th>Ngày tạo</th>
                                <th>Tổng tiền</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($billpros as $key=>$value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td> <a href="{{route('admin.billpro.detail',$value->id)}}">{{$value->code}}</a> </td>
                                <td><a href="{{route('admin.bill.detail',$value->id)}}">@if(isset($value->user)){{$value->user->full_name}} @endif</a></td>
                                <th>{{$value->created_at()}}</th>
                                <td>{{number_format($value->total)}}</td>                               
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-title mb-3" style="padding-left: 14px;">Thống kê doanh thu</div>
                <form action="{!!route('admin.bill.incom')!!}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="row" style="padding-left: 14px;">
                        <div class="col-md-3 form-group mb-3">
                            <label for="start">Từ ngày</label>
                            <input type='date' name='start' class="form-control" @if(isset($start))value="{{$start}}" @endif>
                        </div>
                        <div class="col-md-3 form-group mb-3">
                            <label for="end">Đến ngày</label>
                            <input type='date' name='end' class="form-control" @if(isset($end))value="{{$end}}" @endif>
                        </div>
                        <div class="text-left">
                            <button type="submit" style="margin-top:20px" class="btn btn-m btn-success">Chọn</button>
                        </div>
                    </div>
                </form>
                <div class="col-sm-4">
                    <label style="font-size:14pt">Tổng thu: @if(isset($sum)){{number_format($sum)}} đ @endif</label><br/>
                    <label style="font-size:14pt">Tổng chi: @if(isset($sums)){{number_format($sums)}} đ @endif </label>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop

