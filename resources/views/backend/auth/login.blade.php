@extends('backend.layout.auth')
@section('content')

<!-- WRAPPER -->
<div class="auth-layout-wrap" style="background-image: url(assets/images/photo-wide-4.jpg)">
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-12">
                    <div class="p-4">
                        <h1 class="mb-3 text-18">Đăng nhập hệ thống</h1>
                        <form id="login" action="{!! route('postLogin') !!}" method="post">
                            <fieldset>
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="user_name" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                @if(Session::has('error'))
                                <div class='alert alert-danger'>
                                    <p>{!! Session::get('error') !!}</p>                       
                                </div>
                                @endif 

                                <!-- Change this to a button or input when using this as a form -->
                                <div class="margin-top-30">
                                    <button type="submit" class="btn btn-primary">Đăng nhập</button>
                                </div>

                            </fieldset>
                        </form>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END WRAPPER -->

@stop

