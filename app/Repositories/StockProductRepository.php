<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class StockProductRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\StockProduct';
    }
    public function getAll($request,$id) {
        $query = $this->model;
        if (!is_null($request->get('orderBy'))) {
            $query = $query->orderBy('name', $request->get('orderBy'));
        }
        if (!is_null($request->get('keyword'))) {
            $query = $query->where('name', 'LIKE', '%' . $request->get('keyword') . '%');
        }   
        $stocks = $query->where('stock_id',$id)->orderBy('id', 'ASC')->get();
        return $stocks;
    }  
    public function check($stock_id,$product_id){
        return $this->model->where([['stock_id',$stock_id],['product_id',$product_id]])->first();
    }
    public function whereare($product_id,$stock_id){
        return $this->model->where('product_id',$product_id)->where('stock_id',$stock_id)->first();
    }
}


