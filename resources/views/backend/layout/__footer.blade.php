
<script src="{!!asset('assets/js/vendor/jquery-3.3.1.min.js')!!}"></script>
<script src="{!!asset('assets/js/simple.money.format.js')!!}"></script>
<script src="{!!asset('assets/js/vendor/bootstrap.bundle.min.js')!!}"></script>
<script src="{!!asset('assets/js/es5/script.min.js')!!}"></script>
<script src="{!!asset('assets/js/vendor/perfect-scrollbar.min.js')!!}"></script>

<script src="{!!asset('assets/js/vendor/sweetalert2.min.js')!!}"></script>
<script src="{!!asset('assets/js/bootstrap-datetimepicker.min.js')!!}"></script>
<script src="{!!asset('assets/js/jquery.datetimepicker.full.js')!!}"></script>

<script src="{!!asset('assets/js/modal.script.js')!!}"></script>
<script src="{!!asset('assets/js/sweetalert.script.js')!!}"></script>
<script src="{!!asset('assets/js/vendor/datatables.min.js')!!}"></script>
<script src="{!!asset('assets/js/datatables.script.js')!!}"></script>
<script src="{!!asset('assets/js/custom.js')!!}"></script>
<script src="{!!asset('assets/js/vendor/picker/picker.min.js')!!}"></script>
<script src="{!!asset('assets/js/form.basic.script.js')!!}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

<script type="text/javascript">
    $('.money').simpleMoneyFormat();</script>


<script type="text/javascript">
    $(document).ready(function() {
// Create two variable with the names of the months and days in an array
//    var monthNames = [ "tháng 1", "tháng 2", "tháng 3", "tháng 4", "tháng 5", "tháng 6", "tháng 7", "tháng 8", "tháng 9", "tháng 10", "tháng 11", "tháng 12" ];
    var dayNames = ["Chủ nhật", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7"]

// Create a newDate() object
            var newDate = new Date();
// Extract the current date from Date object
    newDate.setDate(newDate.getDate());
// Output the day, date, month and year   
//    $('#Date').html(dayNames[newDate.getDay()] + ", ngày " + newDate.getDate() + ', ' + monthNames[newDate.getMonth()] + ', năm ' + newDate.getFullYear());
    setInterval(function() {
    // Create a newDate() object and extract the seconds of the current time on the visitor's
    var seconds = new Date().getSeconds();
    // Add a leading zero to seconds value
    $("#sec").html((seconds < 10 ? "0" : "") + seconds);
    }, 1000);
    setInterval(function() {
    // Create a newDate() object and extract the minutes of the current time on the visitor's
    var minutes = new Date().getMinutes();
    // Add a leading zero to the minutes value
    $("#min").html((minutes < 10 ? "0" : "") + minutes);
    }, 1000);
    setInterval(function() {
    // Create a newDate() object and extract the hours of the current time on the visitor's
    var hours = new Date().getHours();
    // Add a leading zero to the hours value
    $("#hours").html((hours < 10 ? "0" : "") + hours);
    }, 1000);
    });
</script>