@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Phân quyền</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li>Phân quyền</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<!-- end of row -->

<div class="row mb-4">
    <div class="col-md-12 mb-4">
        <div class="card text-left">
            <div class="card-body">
                <div class="header">
                <h4 class="card-title mb-3">Danh sách phân quyền</h4>
                </div>
                <div class="table-responsive">
                    <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tên route</th>
                                <th>Tác vụ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($records as $key=>$value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value->name}}</td>
                                <td>
                                    <a href="{{route('admin.role.edit',$value->id)}}" class="btn btn-sm btn-outline-secondary" title="Edit"><i class="nav-icon i-Pen-2 font-weight-bold"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Default Size -->
@stop
