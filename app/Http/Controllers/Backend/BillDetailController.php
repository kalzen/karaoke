<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\BillDetailRepository;
use Repositories\BillRepository;
use Repositories\ProductRepository;
use Repositories\StockProductRepository;
use \Illuminate\Support\Facades\DB;
use App\Bill;
use App\Room;

class BillDetailController extends Controller {

    public function __construct(StockProductRepository $stockproductRepo, BillDetailRepository $billRepo, BillRepository $billsRepo, ProductRepository $productRepo) {
        $this->billRepo = $billRepo;
        $this->billsRepo = $billsRepo;
        $this->productRepo = $productRepo;
        $this->stockproductRepo = $stockproductRepo;
    }

    public function create(Request $request) {
        $datas = $request->all();
        $data_new = explode('&', $datas['data']);
        foreach ($data_new as $key => $val) {
            $converted = explode('=', $val);
            $data[$converted[0]] = $converted[1];
        }
        $input = array();
        $input['bill_id'] = $datas['bill_id'];
        $bill_id = $datas['bill_id'];
        $bills = $this->billsRepo->find($bill_id);
        foreach ($data as $key => $val) {
            if ($key != 'bill_id' && $key != 'zero_configuration_table_length' && $val != 0) {
                if ($this->billRepo->getwhere($bill_id, $key)) {
                    $value = $this->billRepo->getwhere($bill_id, $key);
                    $input['number'] = $val + $value->number;
                    $this->billRepo->update($input, $value->id);
                } else {
                    $input['product_id'] = $key;
                    $input['number'] = $val;
                    $this->billRepo->create($input);
                }
            }
            $stock = $this->stockproductRepo->whereare($key, 1);
            if ($stock) {
                $inputs['number'] = $stock->number - $val;
                $this->stockproductRepo->update($inputs, $stock->id);
            }
        }
        return response()->json(['sucess' => true, 'bill_id' => $bill_id]);
    }

    public function statis() {
        return view('backend/stock/detail');
    }

    public function statistical(Request $request) {
        $bill_ids = Bill::join('room', 'room.id', '=', 'bill.room_id')
                        //->where('room.position', $request->get('position'))
                        ->whereDate('bill.created_at', $request->get('day'))
                        ->select('bill.*')
                        ->pluck('bill.id')->toArray();
        $each_product = DB::table('bill_detail')
                ->select(DB::raw('sum(number) as product_sum, product_id'))
                ->whereIn('bill_id', $bill_ids)
                ->groupBy('product_id')
                ->get();
        foreach ($each_product as $key => $value) {
            $value->product = $this->productRepo->find($value->product_id);
        }
        $search = $request->all();
        $day = date("d/m/Y", strtotime($search['day']));
        return view('backend/stock/detail', compact('each_product', 'day'));
    }

}
