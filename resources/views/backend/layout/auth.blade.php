<!DOCTYPE html>
<html lang="en">
    <head>
        @include('backend/layout/__head')
    </head>

    <body>
        <!-- Page container -->
        @yield('content')   
        <!-- Footer -->
    </body>
</html>

