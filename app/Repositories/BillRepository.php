<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class BillRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Bill';
    }

    public function validateCreate() {
        return $rules = [
            'name' => 'required|unique:product',
        ];
    }

    public function validateUpdate() {
        return $rules = [
            'name' => 'required',
        ];
    }

    public function getAll($request) {
        $query = $this->model;
        if (!is_null($request->get('orderBy'))) {
            $query = $query->orderBy('name', $request->get('orderBy'));
        }
        if (!is_null($request->get('keyword'))) {
            $query = $query->where('name', 'LIKE', '%' . $request->get('keyword') . '%');
        }
        $product = $query->whereIn('status', [0, 1, 3])->orderBy('created_at', 'DESC')->get();
        return $product;
    }

    public function updateTime() {
        $query = $this->model;
        $now = now();
        $bill = \App\Bill::where('start_hour', '<=', $now)->where('status', '=', 2)
                ->get();
        $input['status'] = \App\Bill::PEDING;
        foreach ($bill as $value) {
            $this->model->find($value->id)
                    ->update($input);
        }
        return $bill;
    }

    public function confirmInvoice($id) {
        $this->model->where('id', $id)->update(['status' => 0]);
        return true;
    }

    public function month($month) {
        return $this->model->whereMonth('created_at', $month)->get();
    }

    public function time($start, $end) {
        $query = $this->model;
        if ($start != null) {
            $query = $query->whereDate('created_at', '>=', $start);
        }
        if ($end != null) {
            $query = $query->whereDate('created_at', '<=', $end);
        }
        return $query->get();
    }

    public function getMaxValue() {
        return $this->model->max('code');
    }

    public function cancel($id) {
        $this->model->where('id', $id)->update(['status' => \App\Bill::CANCEL]);
        return true;
    }

}
