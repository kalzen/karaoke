@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Quản lí phòng</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li>Quản lí phòng</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<!-- end of row -->

<div class="row mb-4">
    <div class="col-md-12 mb-4">
        <div class="card text-left">
            <div class="card-body">
                <div class="header">
                <h4 class="card-title mb-3">Danh sách phòng</h4>
                <ul class="header-dropdown">
                    <li><a href="{{route('admin.room.create')}}" class="btn btn-info">Thêm</a></li>
                </ul>
                </div>
                <div class="table-responsive">
                    <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Số phòng</th>
                                <th>Giá phòng</th>
                                <th>Tầng</th>
                                <th>Tác vụ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rooms as $key=>$value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value->name}}</td>
                                <td>{{number_format($value->price)}}</td>
                                <td>{{$value->position}}</td>
                                <td>
                                    <a href="{{route('admin.room.edit',$value->id)}}" class="btn btn-sm btn-outline-secondary" title="Edit"><i class="nav-icon i-Pen-2 font-weight-bold"></i></a>
                                    <a href="{{route('admin.room.destroy',$value->id)}}" class="btn btn-sm btn-outline-danger js-sweetalert" title="Delete" data-type="confirm"><i class="nav-icon i-Close-Window font-weight-bold"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Default Size -->
@stop
