@extends('backend.layout.master')
@section('content')
<div class="container-fluid">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> Quản lí tài khoản</h2>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin.index')}}"><i class="icon-home"></i></a></li>                            
                    <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Trang chủ</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.user.index',$user->role_id)}}">Quản lí tài khoản</a></li>
                </ul>
            </div>            
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class='card'>
                <div class='body'>
                    <h3>Thông tin tài khoản</h3>
                </div>
                <table class="table table-hover js-basic-example dataTable table-custom table-striped m-b-0 c_list">                  
                    <tbody>    
                        @if($user->role_id==3)
                        <tr>                     
                            <td>
                                <h6 class="mb-0">Họ tên:</h6>
                            </td>
                            <td><span>{{$user->full_name}}</span></td>
                        </tr>   
                        <tr>
                             <td>
                                <h6 class="mb-0">Ngày sinh:</h6>
                            </td>
                            <td><span>{{$user->date}}</span></td>
                        </tr>
                        <tr>
                             <td>
                                <h6 class="mb-0">Số điện thoại:</h6>
                            </td>
                            <td><span>{{$user->phone}}</span></td>
                        </tr>
                        <tr>
                             <td>
                                <h6 class="mb-0">Địa chỉ nhà:</h6>
                            </td>
                            <td><span>{{$user->address}}</span></td>
                        </tr>
                        <tr>
                             <td>
                                <h6 class="mb-0">Email:</h6>
                            </td>
                            <td><span>{{$user->email}}</span></td>
                        </tr>
                        @elseif($user->role_id==2)
                        <tr>                     
                            <td>
                                <h6 class="mb-0">Tên công ty/Chủ tàu:</h6>
                            </td>
                            <td><span>{{$user->company_name}}</span></td>
                        </tr>   
                        <tr>
                             <td>
                                <h6 class="mb-0">Địa chỉ:</h6>
                            </td>
                            <td><span>{{$user->address}}</span></td>
                        </tr>
                        <tr>
                             <td>
                                <h6 class="mb-0">Số điện thoại:</h6>
                            </td>
                            <td><span>{{$user->phone}}</span></td>
                        </tr>
                        <tr>
                             <td>
                                <h6 class="mb-0">Email:</h6>
                            </td>
                            <td><span>{{$user->email}}</span></td>
                        </tr>
                        <tr>
                             <td>
                                <h6 class="mb-0">Người đại diện:</h6>
                            </td>
                            <td><span>{{$user->full_name}}</span></td>
                        </tr>
                         <tr>
                             <td>
                                <h6 class="mb-0">Số điện thoại người đại diện:</h6>
                            </td>
                            <td><span>{{$user->phone_profile}}</span></td>
                        </tr>
                        <tr>
                             <td>
                                <h6 class="mb-0">Email người đại diện:</h6>
                            </td>
                            <td><span>{{$user->email_profile}}</span></td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop