<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\CvRepository;
use Repositories\ShipCategoryRepository;

class CvController extends Controller {

    public function __construct(CvRepository $cvRepo,ShipCategoryRepository $shipRepo) {
        $this->cvRepo = $cvRepo;
        $this->shipRepo=$shipRepo;
    }

    public function create(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->cvRepo->validateAPICreate());
        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return response()->json([
                        'error' => 'true',
                        'log' => $error
            ]);
        }
        if ($this->cvRepo->create($input)) {
            return response()->json(['error' => false, 'log' => 'Thêm thành công']);
        }
    }

    public function edit($id) {
        $data = $this->cvRepo->find($id);
        $ship=$this->shipRepo->find($data->ship_id);
        $data->ship=$ship->name;
        if($data){
        return response()->json(['error' => false, 'data' => $data]);}
        else{
            return response()->json(['error'=>true,'log'=>'Dữ liệu không tồn tại']);
        }
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        if ($this->cvRepo->update($input, $id)) {
            return response()->json(['error' => false, 'log' => 'Cập nhật thành công']);
        }
    }

}
