@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li><a href="{{route('admin.bill.index')}}">Quản lí hóa đơn</a></li>
        <li>Chỉnh sửa hóa đơn</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card-body">
            <div class="container">
                <!-- Block 1 -->
                <h2>Danh mục món ăn</h2>
                <form action="{{route('admin.bill.update',$id)}}" method="POST">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Mã món</th>
                                <th>Tên món</th>
                                <th>Số lượng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bill->billdetail as $value)
                            <tr>
                                <td class="col-id">{{$value->product->id}}</td>
                                <td class="col-name">{{$value->product->name}}</td>
                                <td class="col-price"><input type="text" class="put" name="{{$value->product->id}}" value="{{$value->number}}"></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <input type="hidden" name="bill_id" id="bills" value="{{$bill->id}}">

                    <button type="submit" id='order' class="btn btn-success m-1"> Xác nhận </button>
                    <!-- Block 2 -->
                </form>
            </div>
        </div>                          
    </div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop
@section('script')
@parent
<script>
    $(".col-price").append('<button type="button" style="width: 40px;height: 30px;margin-bottom: 3px;" data-id=1 class="but btn btn-success"> <i class="nav-icon i-Up"></i></button>\n\
                         <button type="button" style="width: 40px;height: 30px;margin-bottom: 3px;" data-id=2 class="but btn btn-danger"> <i class="nav-icon i-Down"></i></button>');
    var table = $('#zero_configuration_table').DataTable();
    $("#order").click(function () {
        var data = table.$('input').serialize();
        var bill_id = $('#bills').val();
        var bill='1';
        $.ajax({
            url: "/api/createOrder",
            method: "POST",
            data: {data: data,
                _token: '{!!csrf_token()!!}',
                bill_id: bill_id,
                bill:bill
            },
            success: function (response) {
                if (response.sucess == true) {
                    location.href = '/';
                }
            }
        });
    });
</script>
@stop