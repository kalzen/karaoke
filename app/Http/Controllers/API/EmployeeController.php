<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\EmployeeRepository;

class EmployeeController extends Controller
{
    public function __construct(EmployeeRepository $employeeRepo) {
        $this->employeeRepo = $employeeRepo;
    }
    public function create(Request $request){
        $input=$request->all();
        if($this->employeeRepo->create($input)){
        return response()->json(['error'=>false, 'log' => 'Thêm tin thành công']);
        }
    }
    public function edit($id){
        if($data=$this->employeeRepo->find($id)){
        return response()->json(['error'=>false,'data'=>$data]);}
        else{
            return response()->json(['error'=>true,'log'=>'Dữ liệu không tồn tại']);
        }
    }
    public function update(Request $request,$id){
        $input=$request->all();
        if($this->employeeRepo->update($input,$id)){
        return response()->json(['error'=>false, 'log' => 'Cập nhật thành công']);
        }
    }
    public function all(Request $request){
        $user_id=$request->get('user_id');
        if($data=$this->employeeRepo->getUser($user_id)){
        return response()->json(['error'=>false,'data'=>$data]);}
        else{
            return response()->json(['error'=>true,'log'=>'Dữ liệu không tồn tại']);
        }
    }
}
