<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    const ACTIVE = 0;
    const PEDING = 1;
    const BOOK = 2;
    const CANCEL = 3;
    
    protected $table='bill';
    protected $fillable=['code','room_id','phone','name','user_id','number','start_hour','end_hour','surcharge','detail_id','history','status','total'];

    
    public function created_at() {
        return date( "d/m/Y", strtotime($this->created_at));
    }
    public function updated_at() {
        return date( "d/m/Y", strtotime($this->updated_at));
    }
    public function billdetail(){
    	return $this->hasMany('App\BillDetail','bill_id','id');
    }
     public function room() {
        return $this->belongsTo('App\Room','room_id','id');
    }
     public function user() {
        return $this->belongsTo('App\User','user_id','id');
    }
}
