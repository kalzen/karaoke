<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class BillProductRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\BillProduct';
    }

    public function getAll() {
        return $this->model->orderBy('created_at', 'ASC')->get();
    }

    public function month($month) {
        return $this->model->whereMonth('created_at', $month)->get();
    }

    public function time($start, $end) {
        $query = $this->model;
        if ($start != null) {
            $query = $query->whereDate('created_at', '>=', $start);
        }
        if ($end != null) {
            $query = $query->whereDate('created_at', '<=', $end);
        }
        return $query->get();
    }

    public function today($date) {
        return $this->model->whereDate('created_at', '=', $date)->get();
    }

    public function getMaxValue() {
        return $this->model->max('code');
    }

}
