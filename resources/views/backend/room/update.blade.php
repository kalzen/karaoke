@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li><a href="{{route('admin.room.index')}}">Quản lí phòng</a></li>
        <li>Chỉnh sửa phòng</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class='card mb-4'>
            <div class='card-body'>
                <div class="card-title mb-3">Chỉnh sửa phòng</div>
                <form action="{!!route('admin.room.update',$room->id)!!}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="row  mb-3 clearfix">
                        <div class="col-sm-4">
                            <label>Tên</label>
                            <input type="text" class="form-control" name="name" value="{{$room->name}}">
                            {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                        </div> 
                        <div class="col-sm-4">
                            <label>Giá</label>
                            <input type="text" class="form-control money" name="price" value="{{$room->price}}">
                        </div> 
                        <div class="col-sm-4">                        
                            <label>Tầng</label>
                            <input type="number" class="form-control" name="position" value="{{$room->position}}">
                        </div>
                    </div>
                    <!-- Personal Detail & Address -->
                    <div class="text-left">
                        <button type="submit" class="btn btn-m btn-success">Lưu lại</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop

