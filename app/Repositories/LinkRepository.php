<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class LinkRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Link';
    }
     public function validateCreate() {
        return $rules = [
            'title' => 'required|unique:link',
      
        ];
    }

    public function validateUpdate() {
        return $rules = [
            'title' => 'required',     
        ];
    }
    public function getAll($request) {
        $query = $this->model;
        if (!is_null($request->get('orderBy'))) {
            $query = $query->orderBy('title', $request->get('orderBy'));
        }
        if (!is_null($request->get('keyword'))) {
            $query = $query->where('title', 'LIKE', '%' . $request->get('keyword') . '%');
        }   
        $links = $query->orderBy('id', 'ASC')->get();
        return $links;
    }
   
    
}


