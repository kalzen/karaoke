@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li><a href="{{route('admin.product.index')}}">Quản lí menu</a></li>
        <li>Chỉnh sửa menu</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class='card mb-4'>
            <div class='card-body'>
                <div class="card-title mb-3">Chỉnh sửa menu</div>
                <form action="{!!route('admin.product.update',$product->id)!!}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="row  mb-3 clearfix">
                        <div class="col-sm-3">
                            <label>Tên</label>
                            <input type="text" class="form-control" name="name" value="{{$product->name}}">
                            {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                        </div> 
                         <div class="col-sm-3">
                            <label>Đơn vị tính</label>
                            <input type="text" class="form-control" name="unit" value="{{$product->unit}}">
                             {!! $errors->first('unit', '<span class="text-danger">:message</span>') !!}
                        </div>
                        <div class="col-sm-3">
                            <label>Giá bán</label>
                            <input type="text" class="form-control money" name="price" value="{{$product->price}}">
                        </div>
                        <div class="col-sm-3">
                            <label>Loại</label>
                            <select class="form-control" name="type"> 
                                <option value="1" @if($product->type==1) selected @endif>Có trong kho</option>
                                <option value="2" @if($product->type==2) selected @endif>Không có trong kho</option>
                            </select>
                        </div>
                    </div>
                    <div class="text-left">
                        <button type="submit" class="btn btn-m btn-success">Lưu lại</button>
                    </div>
                </form>
            </div>
            <!-- Personal Detail & Address -->
        </div>
    </div>
</div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop

