@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li><a href="{{route('admin.bill.index')}}">Quản lí hóa đơn</a></li>
        <li>Tạo hóa đơn</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row mb-4">
    <div class="col-md-12 mb-4">
        <div class="card text-left">
            <div class="card-body">
                <div class="header">
                    <h4 class="card-title mb-3">Danh sách hóa đơn</h4>
                </div>
                <div class="table-responsive">
                    <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Mã hóa đơn</th>
                                <th>Trạng thái</th>
                                <th>Khách hàng</th>
                                <th>Phòng</th>
                                <th>Ngày tạo</th>
                                <th>Tổng tiền</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bills as $key=>$value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>@if($value->status == 1 && $value->total == null){{$value->code}} @else <a href="{{route('admin.bill.invoice',$value->id)}}">{{$value->code}}</a> @endif</td>
                                <td>
                                    @if($value->status == 0)
                                    <span class="badge badge-pill badge-success p-2 m-1">Đã thanh toán</span>
                                    @elseif($value->status == 1 && $value->total == null)
                                    <span class="badge badge-pill badge-gray-400 p-2 m-1">Chưa thanh toán</span>
                                    @elseif($value->status == 1)
                                    <span class="badge badge-pill badge-gray-400 p-2 m-1">Đang chờ</span>
                                    @elseif($value->status == 3)
                                    <span class="badge badge-pill badge-danger p-2 m-1">Đã hủy</span>
                                    @endif
                                </td>
                                <td><a href="{{route('admin.bill.invoice',$value->id)}}">{{$value->name}}</a></td>
                                <td>{{$value->room->name}}</td>
                                <th>{{$value->created_at()}}</th>
                                <td>{{number_format($value->total)}}</td>                               
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop