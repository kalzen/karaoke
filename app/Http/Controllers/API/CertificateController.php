<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\CertificateRepository;

class CertificateController extends Controller
{
     public function __construct(CertificateRepository $certificateRepo){
        $this->certificateRepo=$certificateRepo;
    }
    public function create(Request $request){
        $input=$request->all();
        if($this->certificateRepo->create($input)){
        return response()->json(['error' => false,'log' => 'Thêm thành công']);
    }
    }
    public function edit($id){
        if($data=$this->certificateRepo->find($id)){
        return response()->json(['error'=>false,'data' => $data]);}
        else{
            return response()->json(['error'=>true,'log'=>'Dữ liệu không tồn tại']);
        }
    }
    public function update(Request $request,$id){
        $input=$request->all();
        if($this->certificateRepo->update($input,$id)){
            return response()->json(['error'=>false,'log'=>'Cập nhật thành công']);
        }
    }
    public function all(Request $request){
        $user_id=$request->get('user_id');
        if($data=$this->certificateRepo->getUser($user_id)){
        return response()->json(['error'=>false,'data'=>$data]);}
        else{
            return response()->json(['error'=>true,'log'=>'Dữ liệu không tồn tại']);
        }
    }
}
