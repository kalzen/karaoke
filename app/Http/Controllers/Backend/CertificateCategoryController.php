<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CertificateCategory;
use Repositories\CertificateCategoryRepository;
class CertificateCategoryController extends Controller
{
    public function __construct(CertificateCategoryRepository $categoryRepo){
        $this->categoryRepo=$categoryRepo;
    }
    public function index(Request $request){
        $categorys=$this->categoryRepo->getAll($request);
        return view('backend/certificatecategory/index',compact('categorys'));
    }
     public function create() {
        return view('backend/certificatecategory/create');
    }
    public function store(Request $request){
        $input=$request->all();
        $this->categoryRepo->create($input);
        return redirect()->route('admin.certificatecategory.index')->with('success', trans('base.add_success'));
    }
     public function edit($id) {
        $category = $this->categoryRepo->find($id);
        return view('backend/certificatecategory/update', compact('category'));
    }

    public function update(Request $request, $id) {
        $category = $this->categoryRepo->find($id);
        $input = $request->all();
        $this->categoryRepo->update($input, $id);
        return redirect()->route('admin.certificatecategory.index')->with('success', trans('base.update_success'));
    }

    public function destroy($id) {
        $this->categoryRepo->delete($id);
        return redirect()->back()->with('success', trans('base.delete_success'));
    }
}
