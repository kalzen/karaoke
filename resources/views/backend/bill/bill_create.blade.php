@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li><a href="{!!route('admin.bill.in')!!}">Quản lí phiếu chi</a></li>
        <li>Thêm phiếu chi</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class='card mb-4'>
            <div class='card-body'>
                <div class="card-title mb-3">Thêm phiếu chi</div>
                @if (Session::has('false'))
                <div class="alert bg-success alert-styled-left">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold">{{ Session::get('false') }}</span>
                </div>
                @endif
                <form action="{!!route('admin.billpro.create')!!}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="row  mb-3 clearfix">
                        <div class="col-sm-8">
                            <label>Lý do chi</label>
                            <input type="text" class="form-control" name="log" value="{!!old('log')!!}">
                             {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                        </div> 
                        <div class="col-sm-4">
                            <label>Tổng tiền</label>
                            <input type="text" class="form-control money" name="total">
                        </div>
                    </div>
                    <!-- Personal Detail & Address -->
                    <div class=" text-left">
                        <button type="submit" class="btn btn-m btn-success">Lưu lại</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop

