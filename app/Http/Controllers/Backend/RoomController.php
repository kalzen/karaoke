<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\RoomRepository;

class RoomController extends Controller {

    public function __construct(RoomRepository $roomRepo) {
        $this->roomRepo = $roomRepo;
    }

    public function index(Request $request) {
        $rooms = $this->roomRepo->getAll($request);
        return view('backend/room/index', compact('rooms'));
    }

    public function create() {
        return view('backend/room/create');
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->roomRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->with('false', trans('Tên đã tồn tại,mời nhập tên khác'));
        }
        $input['price'] = str_replace(',', '', $request->get('price'));
        $this->roomRepo->create($input);
        return redirect()->route('admin.room.index')->with('success', trans('base.add_success'));
    }

    public function edit($id) {
        $room = $this->roomRepo->find($id);
        return view('backend/room/update', compact('room'));
    }

    public function update(Request $request, $id) {
        $room = $this->roomRepo->find($id);
        $input = $request->all();
        $input['price'] = str_replace(',', '', $request->get('price'));
        $this->roomRepo->update($input, $id);
        return redirect()->route('admin.room.index')->with('success', trans('base.update_success'));
    }

    public function destroy($id) {
        $this->roomRepo->delete($id);
        return redirect()->back()->with('success', trans('base.delete_success'));
    }

}
