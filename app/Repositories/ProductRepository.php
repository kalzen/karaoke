<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ProductRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Product';
    }

    public function validateCreate() {
        return $rules = [
            'name' => 'required|unique:product',
        ];
    }

    public function validateUpdate() {
        return $rules = [
            'name' => 'required',
        ];
    }

    public function getAll($request = null) {
        $query = $this->model;
        if ($request != null) {
            if (!is_null($request->get('orderBy'))) {
                $query = $query->orderBy('name', $request->get('orderBy'));
            }
            if (!is_null($request->get('keyword'))) {
                $query = $query->where('name', 'LIKE', '%' . $request->get('keyword') . '%');
            }
        }
        $product_stock = \App\StockProduct::where('stock_id', '1')->select('product_id', 'number')->get();
        $arr = [];
        foreach ($product_stock as $stock){
            $arr = array_replace($arr, [$stock->product_id => $stock->number]);
        }
        $product = $query->get();
        foreach($product as $p){
            $p->max = $arr[$p->id];
        }
        return $product;
    }
    
    public function getProductByStockId($id) {
        $stock = \App\Stock::where('id', '=', $id)->first();    
        $products = $stock->products;
        $product_stock = \App\StockProduct::where('stock_id', '=', $id)->select('product_id', 'number')->get();   
        $arr = [];
        foreach ($product_stock as $stock){
            $arr = array_replace($arr, [$stock->product_id => $stock->number]);
        }
        foreach($products as $p){
            $p->max = $arr[$p->id];
        }
        return $products;
    }
    public function getIndex($request){
        $query = $this->model;
        if (!is_null($request->get('orderBy'))) {
            $query = $query->orderBy('title', $request->get('orderBy'));
        }
        if (!is_null($request->get('keyword'))) {
            $query = $query->where('title', 'LIKE', '%' . $request->get('keyword') . '%');
        }   
        $product = $query->orderBy('id', 'ASC')->get();
        return $product;
    }
    
    
    public function getProductByPosition($position) {
        $stock = \App\Stock::where('position', '=', $position)->first();
        $products = $stock->products;
        return $products;
    }
    public function getType(){
        $product = $this->model->where('type',1)->get();
        return $product;
    }
}
