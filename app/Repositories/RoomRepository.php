<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class RoomRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Room';
    }
     public function validateCreate() {
        return $rules = [
            'name' => 'required|unique:room',   
        ];
    }

    public function validateUpdate() {
        return $rules = [
            'name' => 'required',     
        ];
    }
    public function getAll($request) {
        $query = $this->model;
        if (!is_null($request->get('orderBy'))) {
            $query = $query->orderBy('name', $request->get('orderBy'));
        }
        if (!is_null($request->get('keyword'))) {
            $query = $query->where('name', 'LIKE', '%' . $request->get('keyword') . '%');
        }   
        $rooms = $query->orderBy('id', 'ASC')->get();
        return $rooms;
    }
 
   
    
}


