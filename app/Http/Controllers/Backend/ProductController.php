<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\ProductRepository;

class ProductController extends Controller {

    public function __construct(ProductRepository $productRepo) {
        $this->productRepo = $productRepo;
    }

    public function index(Request $request) {
        $products = $this->productRepo->getIndex($request);
        return view('backend/product/index', compact('products'));
    }

    public function create() {
        return view('backend/product/create');
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->productRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->with('false', trans('Tên đã tồn tại,mời nhập tên khác'));
        }
        $input['price']=str_replace(',','',$request->get('price'));
        $this->productRepo->create($input);
        return redirect()->route('admin.product.index')->with('success', trans('base.add_success'));
    }

    public function edit($id) {
        $product = $this->productRepo->find($id);
        return view('backend/product/update', compact('product'));
    }

    public function update(Request $request, $id) {
        $product = $this->productRepo->find($id);
        $input = $request->all();
        $input['price']=str_replace(',','',$request->get('price'));
        $this->productRepo->update($input, $id);
        return redirect()->route('admin.product.index')->with('success', trans('base.update_success'));
    }

    public function destroy($id) {
        $this->productRepo->delete($id);
        return redirect()->back()->with('success', trans('base.delete_success'));
    }

}
