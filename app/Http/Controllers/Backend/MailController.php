<?php

namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;

class MailController extends Controller
{
    public function send(Request $request)
    {
        $input = $request->all();
        Mail::send('mailfb', array('name'=>$input["name"],'email'=>$input["email"], 'content'=>$input['content']), function($message){
	        $message->to('hongquan02081996@gmail.com', 'Visitor')->subject('Visitor Feedback!');
	    });

        return view('backend/mail/form');
    }
}
