<?php

namespace App\Http\Controllers\Api;

use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Repositories\UserRepository;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthController extends Controller {

    public function __construct(UserRepository $userRepo) {
        $this->userRepo = $userRepo;
    }

    public function login(Request $request, JWTAuth $JWTAuth) {
        $credentials = $request->only('email', 'password');
        try {
            $token = $JWTAuth->attempt($credentials);
        } catch (JWTException $e) {
            throw new HttpException(500);
        }

        return response()->json([
                            'status' => 'success',
                            'token' => $token,
                            'user' => \Auth::user()
        ]);
    }

    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     * 
     * @param Request $request
     */
    public function logout(Request $request) {
        $this->validate($request, [
            'token' => 'required'
        ]);

        JWTAuth::invalidate($request->input('token'));
    }

}
