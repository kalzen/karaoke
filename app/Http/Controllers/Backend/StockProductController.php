<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\StockProductRepository;
use Repositories\ProductRepository;
use Repositories\StockRepository;
use Repositories\StockLogRepository;
use Repositories\BillProductRepository;
use Illuminate\Support\Facades\Auth;

class StockProductController extends Controller {

    public function __construct(BillProductRepository $billproRepo, StockLogRepository $stockLogRepo, StockProductRepository $stockProductRepo, ProductRepository $productRepo, StockRepository $stockRepo) {
        $this->stockProductRepo = $stockProductRepo;
        $this->productRepo = $productRepo;
        $this->stockRepo = $stockRepo;
        $this->stockLogRepo = $stockLogRepo;
        $this->billproRepo = $billproRepo;
    }

    public function index(Request $request, $id) {
        $stock = $this->stockRepo->find($id);
        $stock_product = $this->stockProductRepo->getAll($request, $id);
        $log = $this->stockLogRepo->getAll();
      
        return view('backend/stock/index', compact('stock', 'stock_product', 'id', 'log'));
    }

    public function export($id) {
        $sub = $this->stockRepo->findSub();
        $product = $this->productRepo->getProductByStockId($id);       
        return view('backend/stock/export', compact('product', 'id', 'sub'));
    }

    public function import($id) {
        $product = $this->productRepo->getType();
        return view('backend/stock/import', compact('product', 'id'));
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->stockProductRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->with('false', trans('Tên đã tồn tại,mời nhập tên khác'));
        }
        $this->stockProductRepo->create($input);
        return redirect()->route('admin.stock.index')->with('success', trans('base.add_success'));
    }

    public function edit($id) {
        $stock = $this->stockProductRepo->find($id);
        return view('backend/stock/update', compact('stock'));
    }

    public function input(Request $request) {
        $datas = $request->all();
        $data_new= explode('&', $datas['data']);
        foreach($data_new as $key=>$val){
            $converted = explode('=',$val);
            $data[$converted[0]] = $converted[1];
        }
        foreach ($data as $key => $value) {
            if ($key !='_token' && $key != 'stock_id' && $value != 0 && $key != 'log' && $key != 'stock_from' && $key !='zero_configuration_table_length') {
                $info[$key] = $value;
            }
        }     
        $inp['product'] = json_encode($info);
        $input = array();
        $input['stock_id'] = $datas['stock_id'];
        $stock_id = $datas['stock_id'];
        $sum = 0;
        $inp['log'] = 'nhap';
        $inp['stock_to'] = $datas['stock_id'];
        $this->stockLogRepo->create($inp);
        foreach ($data as $key => $val) {
            if ($key !='_token' && $key != 'stock_id' && $key != 'log' && $key != 'zero_configuration_table_length' && $val!=0) {
                if (!$this->stockProductRepo->check($stock_id, $key)) {
                    $input['product_id'] = $key;
                    $input['number'] = $val;
                    $this->stockProductRepo->create($input);
                } else {
                    $value = $this->stockProductRepo->whereare($key, $stock_id);
                    $input['number'] = $val + $value->number;
                    $this->stockProductRepo->update($input, $value->id);
                }
                $product = $this->productRepo->find($key);
                $sum = $sum + ($product->input_price * $val);
            }
        }
        return response()->json(['sucess' => true, 'id' => $stock_id]);
    }

    public function output(Request $request) {
        $data = $request->all();
        $info=array();
        foreach($data as $key=>$value){
        if ($key != 'stock_id' && $value != 0 && $key != 'log' && $key != 'stock_from' && $key != 'zero_configuration_table_length') {
            $info[$key]=$value;
        }      
     }
        $inpt['product']=json_encode($info);
        
        $input = array();
        $input['stock_id'] = $data['stock_id'];
        $stock_id = $data['stock_id'];
        $log = $data['log'];
        $inpt['log'] = $data['log'];
        $inpt['stock_from'] = $data['stock_id'];
        if (isset($data['stock_from'])) {
            $inpt['stock_to'] = $data['stock_from'];
        }
        $this->stockLogRepo->create($inpt);
        foreach ($data as $key => $val) {
            if ($key != 'stock_id' && $val != 0 && $key != 'log' && $key != 'stock_from' && $key != 'zero_configuration_table_length') {
                $value = $this->stockProductRepo->whereare($key, $stock_id);
                if ($value) {
                    $input['number'] = $value->number - $val;
                    $this->stockProductRepo->update($input, $value->id);
                }
                if (isset($data['stock_from'])) {
                    $valu = $this->stockProductRepo->whereare($key, $data['stock_from']);
                    $inp['stock_id'] = $data['stock_from'];
                    if ($valu) {
                        $inp['number'] = $valu->number + $val;
                        $this->stockProductRepo->update($inp, $valu->id);
                    } else {
                        $inp['number'] = $val;
                        $inp['product_id'] = $key;
                        $this->stockProductRepo->create($inp);
                    }
                }
            }
        }
        return response()->json(['sucess' => true, 'id' => $stock_id]);
    }
}
