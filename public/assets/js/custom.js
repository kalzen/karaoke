(function () {

    var index = 1;

    $('.btn-choose').on('click', choose);
    $('#bill-table').on('change keyup paste', '.bill-quantity', changeValue);

    function choose() {
        var self = this,
                bill = $('#bill-table'),
                data = getDataMenu($(self).parent().parent()),
                html = createRow(data);
        var bill_id = $("input[name='bill_id']").val();
        if (bill.find('.empty').length) {
            bill.empty();
        }
        bill.append(html);
        $.ajax({
            url: "/api/createOrder",
            method: "POST",
            data: {
                bill_id: bill_id,
                product_id: data.id,
                number: data.sl
            },
            success: function (response) {
                if (response.error == false) {
                    console.log('success');
                }
            },
            error: function (res) {
                console.log(res);
            }
        });
    }

    function changeValue() {
        var self = this,
                row = $(self).parent().parent(),
                data = getDataBill(row);

        row.find('.bill-total').html(calculate(data.price, data.quantity));
    }

    function getDataMenu(row) {
        var id = $(row.find('.col-id')).html(),
                name = $(row.find('.col-name')).html(),
                price = $(row.find('.col-price')).html();
        var sl = $(row.find($("input[name='number']"))).val();
        var data = {
            id: id,
            name: name,
            price: price,
            sl: sl
        }

        return data;
    }

    function getDataBill(row) {
        var price = $(row.find('.bill-price')).html(),
                quantity = $(row.find('.bill-quantity')).val();

        var data = {
            price: price,
            quantity: quantity
        }

        return data;
    }

    function createRow(data) {
        var html = '<tr>';

        html += '<td>' + index++ + '</td>';
        html += '<td>' + data.id + '</td>';
        html += '<td>' + data.name + '</td>';
        html += '<td>' + data.sl + '</td>';
        html += '</tr>';
        return html;
    }

    function calculate(price, quantity) {
        return price * quantity;
    }

})();


$(function () {

//    $(".col-price").append('<button type="button" style="width: 40px;height: 30px;margin-bottom: 3px;" data-id=1 class="but btn btn-success"> <i class="nav-icon i-Up"></i></button>\n\
                         //<button type="button" style="width: 40px;height: 30px;margin-bottom: 3px;" data-id=2 class="but btn btn-danger"> <i class="nav-icon i-Down"></i></button>');
    $('body').delegate('.but','click', function(){
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();
        var max = $button.parent().find("input").data('max');
        var value = this.dataset.id;
        if (value == '1') {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        if (newVal == 0) {
            $(this).parent().parent().addClass('no-print');
        }else{
            $(this).parent().parent().removeClass('no-print');
        }
        if (newVal > max) {
            newVal = max;
        }
        $button.parent().find("input").val(newVal);
    })
    $(".but").click(function () {

        var $button = $(this);
        var oldValue = $button.parent().find("input").val();
        var max = $button.parent().find("input").data('max');
        var value = this.dataset.id;
        if (value == '1') {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        if (newVal == 0) {
            $(this).parent().parent().addClass('no-print');
        }else{
            $(this).parent().parent().removeClass('no-print');
        }
        if (newVal > max) {
            newVal = max;
        }
        $button.parent().find("input").val(newVal);

    });

    $(".stock_log").click(function () {
        var stock_to = this.dataset.id;
        $.ajax({
            url: "/api/outStock/" + stock_to,
            method: "POST",
            data: $('#frminout').serialize(),
            success: function (response) {
                location.href = '/stock/' + response.id;
            }
        });
    });

    //  Validate max
    $('body').delegate('.put','change', function(){
        $(this).next('.err-max').hide();
        var value = $(this).val();

        var max = $(this).data('max');
        if (value == 0) {
            $(this).parent().parent().addClass('no-print');
        }else{
            $(this).parent().parent().removeClass('no-print');
        }
        if (value > max) {
            $(this).val(max);
            $(this).next('.err-max').html('Max size: ' + max);
            $(this).next('.err-max').show();
        }
    });

    $(".btn-destroy-book-room").on("click", function () {
        var bill_id = $(this).data('id');
        swal({
            title: "Bạn có chắc chắn hủy phòng?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#0CC27E",
            cancelButtonColor: "#FF586B",
            confirmButtonText: "Đúng, hủy phòng!",
            cancelButtonText: "Không, trở lại!",
            confirmButtonClass: "btn btn-success mr-5",
            cancelButtonClass: "btn btn-danger",
            buttonsStyling: !1
        }).then(function () {
            $.ajax({
                url: "api/bill/cancel/" + bill_id,
                method: "GET",
                success: function (res) {
                    swal("Đã hủy phòng!", "success");
                    location.href = '/';
                },
                error: function (res) {
                    console.log(res);
                }
            });

        }, function (t) {
            "cancel" === t && swal("Cancelled")
        })
    })
});
