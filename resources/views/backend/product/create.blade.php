@extends('backend.layout.master')
@section('content')
<div class="breadcrumb">
    <h1>Trang chủ</h1>
    <ul>
        <li><a href="{{route('admin.index')}}">Trang chủ</a></li>
        <li><a href="{{route('admin.product.index')}}">Quản lí menu</a></li>
        <li>Thêm menu</li>
    </ul>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class='card mb-4'>
            <div class='card-body'>
                <div class="card-title mb-3">Thêm menu</div>
                @if (Session::has('false'))
                <div class="alert bg-success alert-styled-left">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold">{{ Session::get('false') }}</span>
                </div>
                @endif
                <form action="{!!route('admin.product.store')!!}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <div class="row  mb-3 clearfix">
                        <div class="col-sm-3">
                            <label>Tên</label>
                            <input type="text" class="form-control" name="name" value="{!!old('name')!!}">
                             {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                        </div> 
                        <div class="col-sm-3">
                            <label>Đơn vị tính</label>
                            <input type="text" class="form-control" name="unit" value="{!!old('unit')!!}">
                             {!! $errors->first('unit', '<span class="text-danger">:message</span>') !!}
                        </div>
                        <div class="col-sm-3">
                            <label>Giá bán</label>
                            <input type="text" class="form-control money" name="price">
                        </div>
                         <div class="col-sm-3">
                            <label>Loại</label>
                            <select class="form-control" name="type"> 
                                <option value="1">Có trong kho</option>
                                <option value="2">Không có trong kho</option>
                            </select>
                        </div>
                    </div>
                    <!-- Personal Detail & Address -->
                    <div class=" text-left">
                        <button type="submit" class="btn btn-m btn-success">Lưu lại</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /row -->
<!-- /#page-wrapper -->
@stop

