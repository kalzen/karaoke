<nav class="navbar navbar-default navbar-mobile navbar-fixed light bootsnav">
    <div class="container">

        <!-- Start Logo Header Navigation -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="index.html">
                <img src="assets/img/logo.png" class="logo logo-display" alt="">
                <img src="assets/img/logo.png" class="logo logo-scrolled" alt="">
            </a>

        </div>
        <!-- End Logo Header Navigation -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-menu">

            <ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home</a>
                    <ul class="dropdown-menu animated fadeOutUp">
                        <li><a href="index.html">Home 1</a></li>
                        <li><a href="home-2.html">Home 2</a></li>
                        <li><a href="home-3.html">Home 3</a></li>
                        <li><a href="home-4.html">Home 4</a></li>
                        <li><a href="freelancer.html">Freelancer</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Employer</a>
                    <ul class="dropdown-menu animated fadeOutUp">
                        <li><a href="create-job.html">Create Job</a></li>
                        <li><a href="manage-job.html">Manage Jobs</a></li>
                        <li><a href="employer-detail.html">Employer Detail</a></li>
                        <li><a href="manage-resume.html">Manage Resumes</a></li>
                        <li><a href="resume-detail.html">Resume Detail</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Candidate</a>
                    <ul class="dropdown-menu animated fadeOutUp">
                        <li><a href="browse-job.html">Browse Jobs</a></li>
                        <li><a href="create-company.html">Create Company</a></li>
                        <li><a href="browse-category.html">Browse Categories</a></li>
                        <li><a href="create-resume.html">Create Resume</a></li>
                        <li><a href="manage-resume.html">Manage Resume</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Extra</a>
                    <ul class="dropdown-menu animated fadeOutUp">
                        <li><a href="candidate.html">Candidate</a></li>
                        <li><a href="profile-settings.html">Profile Settings</a></li>
                        <li><a href="employer.html">Employer</a></li>
                        <li><a href="featured-job.html">Featured Job</a></li>
                        <li><a href="checkout.html">Checkout</a></li>
                        <li><a href="job-view-cart.html">Job View Cart</a></li>
                        <li><a href="job-detail-2.html">Job Detail 2</a></li>
                        <li><a href="job-layout-one.html">Job Layout One</a></li>
                        <li><a href="job-layout-two.html">Job Layout Two</a></li>
                        <li><a href="job-layout-three.html">Job Layout Three</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">UI Elements</a>
                    <ul class="dropdown-menu animated fadeOutUp">
                        <li><a href="icons.html">Icons</a></li>
                        <li><a href="404.html">404</a></li>
                        <li><a href="pricing.html">Pricing</a></li>
                        <li><a href="job-detail.html">Job Detail</a></li>
                        <li><a href="blog.html">Blog</a></li>
                        <li><a href="blog-detail.html">Blog Detail</a></li>
                        <li><a href="contact.html">Contact</a></li>
                        <li><a href="component.html">Component</a></li>
                        <li><a href="typography.html">Typography</a></li>
                    </ul>
                </li>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="br-right"><a href="javascript:void(0)"  data-toggle="modal" data-target="#signin"><i class="login-icon ti-user"></i>Login</a></li>
                <li class="sign-up"><a class="btn-signup red-btn" href="signup.html"><span class="ti-briefcase"></span>Create Account</a></li> 
            </ul>

        </div>
        <!-- /.navbar-collapse -->
    </div>   
</nav>