<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Repositories\RoleRepository;

class RoleController extends Controller {

    public function __construct(RoleRepository $roleRepo) {
        $this->roleRepo = $roleRepo;
    }

    public function index() {
        $records = $this->roleRepo->getAllRole();
        return view('backend/role/index', compact('records'));
    }

    public function edit($id) {

        $record = $this->roleRepo->find($id);
        $routes = $this->roleRepo->getRouteName();
//        dd($routes);
        $routed = explode(',', $record->route);
        return view('backend/role/update', compact('record', 'routes', 'id', 'routed'));
    }

    public function update(Request $request, $id) {
        $input = $request->all();
        $input['route'] = implode(',', $input['route']);
        $this->roleRepo->update($input, $id);
        return redirect()->route('admin.role.index')->with('success', trans('backend/base.msg_susscess'));
    }


}
