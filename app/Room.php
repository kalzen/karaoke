<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model {

    protected $table = 'room';
    protected $fillable = ['name', 'price','position','staus'];

    public function created_at() {
        return date("d/m/Y", strtotime($this->created_at));
    }

    public function updated_at() {
        return date("d/m/Y", strtotime($this->updated_at));
    }
    public function bill(){
    	return $this->hasMany('App\Bill','room_id','id');
    }
    

}
