
new Picker(document.querySelector('.js-date-picker'), {
  controls: true,
  format: 'DD-MM-YYYY HH:mm:ss',
  headers: true,
  text: {
    title: 'Chọn giờ đặt',
  },
});