<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */
Route::group(['middleware' => ['api', 'cors']], function() {

    Route::post('/confirm', 'Backend\BillController@confirm');
    Route::post('/createOrder', 'Backend\BillDetailController@create');
    
    Route::post('/outStock/{id}', 'Backend\StockProductController@outputs');
    Route::post('/outStock', 'Backend\StockProductController@output');
    
    Route::get('/bill/cancel/{id}', ['as' => 'api.bill.cancel', 'uses' => 'Backend\BillController@cancel']);
});
