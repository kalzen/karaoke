<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
use Illuminate\Support\Facades\Session;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class BillDetailRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\BillDetail';
    }
     public function validateCreate() {
        return $rules = [
            'name' => 'required|unique:product',   
        ];
    }

    public function validateUpdate() {
        return $rules = [
            'name' => 'required',     
        ];
    }
    public function getAll($request) {
        $query = $this->model;
        if (!is_null($request->get('orderBy'))) {
            $query = $query->orderBy('name', $request->get('orderBy'));
        }
        if (!is_null($request->get('keyword'))) {
            $query = $query->where('name', 'LIKE', '%' . $request->get('keyword') . '%');
        }   
        $product = $query->orderBy('id', 'ASC')->get();
        return $product;
    }
    public function getwhere($id,$product_id){
        return $this->model->where('bill_id',$id)->where('product_id',$product_id)->first();
    }
    
}


