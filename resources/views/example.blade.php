<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Laravel ReactJS </title>
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/frontend/css/app.css') }}">

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap.min.css') }}">
        <!-- Bootstrap Select Option css -->
        <link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap-select.min.css') }}">
        <!-- Icons -->
        <link href="{{ asset('assets/frontend/plugins/icons/css/icons.css') }}" rel="stylesheet">
        <!-- Animate -->
        <link href="{{ asset('assets/frontend/plugins/animate/animate.css') }}" rel="stylesheet">
        <!-- Nice Select Option css -->
        <link rel="stylesheet" href="{{ asset('assets/frontend/plugins/nice-select/css/nice-select.css') }}">
        <!-- Bootsnav -->
        <link href="{{ asset('assets/frontend/plugins/bootstrap/css/bootsnav.css') }}" rel="stylesheet">
        <!-- Aos Css -->
        <link href="{{ asset('assets/frontend/plugins/aos-master/aos.css') }}" rel="stylesheet">
        <!-- Slick Slider -->
        <link href="{{ asset('assets/frontend/plugins/slick-slider/slick.css') }}" rel="stylesheet">	
        <!-- Custom style -->
        <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/frontend/css/responsiveness.css') }}" rel="stylesheet">
        <!-- Custom Color -->
        <link href="{{ asset('assets/frontend/css/skin/default.css') }}" rel="stylesheet">

        <script type="text/javascript">
            window.Laravel = {!! json_encode([
                    'baseUrl' => url('/'),
                    'csrfToken' => csrf_token(),
            ]) !!}
            ;
        </script>
    </head>
    <body>
        <div id="app"></div>

        <footer class="dark-bg footer">
            <div class="container">

                <!-- Row Start -->
                <div class="row">

                    <div class="col-md-8 col-sm-8">
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <h4>Featured Job</h4>
                                <ul>
                                    <li><a href="#">Browse Jobs</a></li>
                                    <li><a href="#">Premium MBA Jobs</a></li>
                                    <li><a href="#">Access Database</a></li>
                                    <li><a href="#">Manage Responses</a></li>
                                    <li><a href="#">Report a Problem</a></li>
                                    <li><a href="#">Mobile Site</a></li>
                                    <li><a href="#">Jobs by Skill</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <h4>Featured Job</h4>
                                <ul>
                                    <li><a href="#">Browse Jobs</a></li>
                                    <li><a href="#">Premium MBA Jobs</a></li>
                                    <li><a href="#">Access Database</a></li>
                                    <li><a href="#">Manage Responses</a></li>
                                    <li><a href="#">Report a Problem</a></li>
                                    <li><a href="#">Mobile Site</a></li>
                                    <li><a href="#">Jobs by Skill</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <h4>Featured Job</h4>
                                <ul>
                                    <li><a href="#">Browse Jobs</a></li>
                                    <li><a href="#">Premium MBA Jobs</a></li>
                                    <li><a href="#">Access Database</a></li>
                                    <li><a href="#">Manage Responses</a></li>
                                    <li><a href="#">Report a Problem</a></li>
                                    <li><a href="#">Mobile Site</a></li>
                                    <li><a href="#">Jobs by Skill</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <h4>Featured Job</h4>
                        <!-- Newsletter -->
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Enter Email">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default"><i class="fa fa-location-arrow font-20"></i></button>
                            </span>
                        </div>

                        <!-- Social Box -->
                        <div class="f-social-box">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook facebook-cl"></i></a></li>
                                <li><a href="#"><i class="fa fa-google google-plus-cl"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter twitter-cl"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest pinterest-cl"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram instagram-cl"></i></a></li>
                            </ul>
                        </div>

                        <!-- App Links -->
                        <div class="f-app-box">
                            <ul>
                                <li><a href="#"><i class="fa fa-apple"></i>App Store</a></li>
                                <li><a href="#"><i class="fa fa-android"></i>Play Store</a></li>
                            </ul>
                        </div>

                    </div>

                </div>

                <!-- Row Start -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright text-center">
                            <p>&copy; Copyright 2018 Job Hill | Powerd By <a href="http://themezhub.com/" title="Themez Hub">Themez Hub</a></p>
                        </div>
                    </div>
                </div>

            </div>
        </footer>
        <script type="text/javascript" src="{{ asset('assets/frontend/js/app.js') }}"></script>
        <!-- =================== START JAVASCRIPT ================== -->
        <!-- Jquery js-->
        <script src="{{ asset('assets/frontend/js/jquery.min.js') }}"></script>
        <!-- Bootstrap js-->
        <script src="{{ asset('assets/frontend/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- Bootsnav js-->
        <script src="{{ asset('assets/frontend/plugins/bootstrap/js/bootsnav.js') }}"></script>
        <script src="{{ asset('assets/frontend/js/viewportchecker.js') }}"></script>
        <!-- Slick Slider js-->
        <script src="{{ asset('assets/frontend/plugins/slick-slider/slick.js') }}"></script>
        <!-- wysihtml5 editor js -->
        <script src="{{ asset('assets/frontend/plugins/bootstrap/js/wysihtml5-0.3.0.js') }}"></script>
        <script src="{{ asset('assets/frontend/plugins/bootstrap/js/bootstrap-wysihtml5.js') }}"></script>
        <!-- Aos Js -->
        <script src="{{ asset('assets/frontend/plugins/aos-master/aos.js') }}"></script>
        <!-- Nice Select -->
        <script src="{{ asset('assets/frontend/plugins/nice-select/js/jquery.nice-select.min.js') }}"></script>
        <!-- Custom Js -->
        <script src="{{ asset('assets/frontend/js/custom.js') }}"></script>
        <script>
            AOS.init();
        </script>
    </body>
</html>