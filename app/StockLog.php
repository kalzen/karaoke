<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockLog extends Model
{
    protected $table = 'stock_log';
    protected $fillable = ['log','stock_from','stock_to','product'];
    
    public function from() {
        return $this->belongsTo('App\Stock','stock_from','id');
    }
    
    public function to() {
        return $this->belongsTo('App\Stock','stock_to','id');
    }
}
