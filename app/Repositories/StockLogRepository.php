<?php

namespace Repositories;

use Repositories\Support\AbstractRepository;
class StockLogRepository extends AbstractRepository {

    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\StockLog';
    }

    
    public function getAll() {
        $all =  $this->all();
        foreach($all as $value){
            $value->stock_from = $value->from?$value->from->name:'Nhập hàng';
            $value->stock_to = $value->to?$value->to->name:'';
            $arr = [];
            foreach(json_decode($value->product) as $key=>$product){
                $name = \App\Product::where('id', '=', $key)->first()->name;
                $arr = array_merge($arr, [$name.': số lượng '.$product]);
            }
            $value->product_log = $arr;
        }
        return $all;
    }

}
