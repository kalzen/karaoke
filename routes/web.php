<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */



/* AUTH */
Route::get('/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('/login', ['as' => 'postLogin', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);
Route::get('/register', ['as' => 'register', 'uses' => 'Auth\AuthController@getRegister']);
Route::post('/register', ['as' => 'postRegister', 'uses' => 'Auth\AuthController@postRegister']);
/* ADMIN */
Route::group(['middleware' => 'admin'], function() {
    Route::get('/', ['as' => 'admin.index', 'uses' => 'Backend\BackendController@index']);
    Route::get('/user', ['as' => 'admin.user.index', 'uses' => 'Backend\UserController@index']);
    Route::get('/user/create', ['as' => 'admin.user.create', 'uses' => 'Backend\UserController@create']);
    Route::post('/user/store', ['as' => 'admin.user.store', 'uses' => 'Backend\UserController@store']);
    Route::get('/user/edit/{id}', ['as' => 'admin.user.edit', 'uses' => 'Backend\UserController@edit']);
    Route::get('/user/resetdata/{id}', ['as' => 'admin.user.resetdata', 'uses' => 'Backend\UserController@resetdata']);
    Route::post('/user/reset/{id}', ['as' => 'admin.user.reset', 'uses' => 'Backend\UserController@reset']);

    Route::get('/user/data/{id}', ['as' => 'admin.user.data', 'uses' => 'Backend\UserController@data']);
    Route::post('/user/update/{id}', ['as' => 'admin.user.update', 'uses' => 'Backend\UserController@update']);
    Route::get('/user/delete/{id}', ['as' => 'admin.user.destroy', 'uses' => 'Backend\UserController@destroy']);

    //Quản lý role
    Route::get('/role', ['as' => 'admin.role.index', 'uses' => 'Backend\RoleController@index']);
    Route::get('/role/edit/{id}', ['as' => 'admin.role.edit', 'uses' => 'Backend\RoleController@edit']);
    Route::post('/role/update/{id}', ['as' => 'admin.role.update', 'uses' => 'Backend\RoleController@update']);

    //Quản lí menu
    Route::get('/product', ['as' => 'admin.product.index', 'uses' => 'Backend\ProductController@index']);
    Route::get('/product/create', ['as' => 'admin.product.create', 'uses' => 'Backend\ProductController@create']);
    Route::post('/product/store', ['as' => 'admin.product.store', 'uses' => 'Backend\ProductController@store']);
    Route::get('/product/edit/{id}', ['as' => 'admin.product.edit', 'uses' => 'Backend\ProductController@edit']);
    Route::post('/product/update/{id}', ['as' => 'admin.product.update', 'uses' => 'Backend\ProductController@update']);
    Route::get('/product/delete/{id}', ['as' => 'admin.product.destroy', 'uses' => 'Backend\ProductController@destroy']);
    //Quản lí phòng
    Route::get('/room', ['as' => 'admin.room.index', 'uses' => 'Backend\RoomController@index']);
    Route::get('/room/create', ['as' => 'admin.room.create', 'uses' => 'Backend\RoomController@create']);
    Route::post('/room/store', ['as' => 'admin.room.store', 'uses' => 'Backend\RoomController@store']);
    Route::get('/room/edit/{id}', ['as' => 'admin.room.edit', 'uses' => 'Backend\RoomController@edit']);
    Route::post('/room/update/{id}', ['as' => 'admin.room.update', 'uses' => 'Backend\RoomController@update']);
    Route::get('/room/delete/{id}', ['as' => 'admin.room.destroy', 'uses' => 'Backend\RoomController@destroy']);
    //Quản lí kho
    Route::get('/stock/{id}', ['as' => 'admin.stock.index', 'uses' => 'Backend\StockProductController@index']);
    Route::get('/stock/export/{id}', ['as' => 'admin.stock.export', 'uses' => 'Backend\StockProductController@export']);
    Route::get('/stock/import/{id}', ['as' => 'admin.stock.import', 'uses' => 'Backend\StockProductController@import']);

    Route::get('/bill', ['as' => 'admin.bill.index', 'uses' => 'Backend\BillController@index']);
    Route::post('/bill/store', ['as' => 'admin.bill.store', 'uses' => 'Backend\BillController@store']);
    Route::get('/bill/edit/{id}', ['as' => 'admin.bill.edit', 'uses' => 'Backend\BillController@edit']);
    Route::get('/bill/editup/{id}', ['as' => 'admin.bill.editup', 'uses' => 'Backend\BillController@editup']);
    Route::post('/bill/update/{id}', ['as' => 'admin.bill.update', 'uses' => 'Backend\BillController@update']);
    Route::get('/bill/detail/{id}', ['as' => 'admin.bill.detail', 'uses' => 'Backend\BillController@detail']);
    Route::get('/bill/confirm/{id}', ['as' => 'admin.bill.confirm', 'uses' => 'Backend\BillController@confirm']);
    Route::get('/bill/invoice/{id}', ['as' => 'admin.bill.invoice', 'uses' => 'Backend\BillController@invoice']);
    Route::get('/bill/in', ['as' => 'admin.bill.in', 'uses' => 'Backend\BillController@in']);
    Route::post('/bill/incom', ['as' => 'admin.bill.incom', 'uses' => 'Backend\BillController@incom']);
    Route::get('/billpro/createin', ['as' => 'admin.billpro.createin', 'uses' => 'Backend\BillController@createin']);
    Route::post('/billpro/create', ['as' => 'admin.billpro.create', 'uses' => 'Backend\BillController@create']);
    Route::get('/billpro/detail/{id}', ['as' => 'admin.billpro.detail', 'uses' => 'Backend\BillController@bill_detail']);
    Route::post('/billdetail/statistical', ['as' => 'admin.billdetail.statistical', 'uses' => 'Backend\BillDetailController@statistical']);
    Route::get('/billdetail', ['as' => 'admin.billdetail.statis', 'uses' => 'Backend\BillDetailController@statis']);
    
    Route::post('/inputStock', 'Backend\StockProductController@input');

    Route::get('/form', function () {
        return view('backend/mail/form');
    });
});

