<?php

return [
    //User
    'phone_number' => 'Số điện thoại',
    'status' => 'Trạng thái',
    'experience_date' => 'Đăng kí trải nghiệm',
    'educate_date' => 'Đăng kí đào tạo',
    'number_user' => 'Số user',
    'cancel' => 'Hủy',
    'change_password' => 'Đổi mật khẩu',
    'add_user' => 'Thêm tài khoản',
    'date_created_from' => 'Từ ngày',
    'date_created_to' => 'Đến ngày',
    'export' => 'Xuất danh sách',
    'subject' => 'Môn học',
    'lesson' => 'Bài học',
    'hot' => 'Hot',
    'save' => 'Lưu',
    'user_joined_exam'=>'Tài khoản tham gia đề thi'
];
