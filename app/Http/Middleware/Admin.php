<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class Admin {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $current_route = $request->route()->getName();
        $warehouse = \App\Stock::all();
        \View::share('warehouse', $warehouse);
        if (!is_null(Auth::user())) {
            $route = Auth::user()->role->route;
            $now = \Carbon\Carbon::now()->format('H:i:s');
            if ($route == "all" || in_array($current_route, explode(',', $route))) {
                if (Auth::user()->start <= $now && Auth::user()->end >= $now) {
                    return $next($request);
                } else {
                    abort(403);
                }
            } else {
                abort(403);
            }
        } else {
            return redirect()->route('login');
        }
    }

}
