<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Repositories\BillDetailRepository;
use Repositories\BillRepository;
use Repositories\ProductRepository;
use Repositories\BillProductRepository;
use Repositories\StockProductRepository;
use Repositories\StockRepository;

class BillController extends Controller {

    public function __construct(StockProductRepository $stockproductRepo, StockRepository $stockRepo, BillProductRepository $billproRepo, BillRepository $billRepo, BillDetailRepository $detailRepo, ProductRepository $productRepo) {
        $this->billRepo = $billRepo;
        $this->detailRepo = $detailRepo;
        $this->productRepo = $productRepo;
        $this->billproRepo = $billproRepo;
        $this->stockproductRepo = $stockproductRepo;
        $this->stockRepo = $stockRepo;
    }

    public function index(Request $request) {
        $bills = $this->billRepo->getAll($request);
        return view('backend/bill/index', compact('bills'));
    }

    public function store(Request $request) {
        $input = $request->all();
        $validator = \Validator::make($input, $this->billRepo->validateCreate());
        if ($validator->fails()) {
            return redirect()->back()->with('false', trans('Tên đã tồn tại,mời nhập tên khác'));
        }
        if (!$request->get('start_hour')) {
            $input['start_hour'] = now();
        } else {
            $input['start_hour'] = date("Y-m-d H:i:m", strtotime($request->get('start_hour')));
        }
        $code = $this->billRepo->getMaxValue();
        if (is_null($code)) {
            $code = 'HP-' . date("dmy") . '-00000';
        }
        $input['code'] = ++$code;
        $input['user_id'] = \Auth::user()->id;
        $this->billRepo->create($input);
        return redirect()->route('admin.index')->with('success', trans('base.book_room_success'));
    }

    public function edit($id) {
        $bill = $this->billRepo->find($id);
        $sum = 0;
        foreach ($bill->billdetail as $value) {
            $sum = $sum + ($value->product->price * $value->number);
        }
        $total = $sum + (((strtotime(date($bill->end_hour)) - strtotime(date($bill->start_hour))) / 3600) * $bill->room->price);
        $bill->total = $total;
        return view('backend/bill/detail', compact('bill'));
    }

    public function detail($id) {
        $bill = $this->billRepo->find($id);
        $product = $this->productRepo->getProductByStockId(1);
        $products = $this->productRepo->whereBy('type', 2);
        foreach ($products as $key => $value) {
            $value->max = '999999';
        }
        return view('backend/bill/detail', compact('bill', 'product', 'products'));
    }

    public function editup($id) {
        $bill = $this->billRepo->find($id);
        return view('backend/bill/update', compact('bill', 'id'));
    }

    public function update(Request $request, $id) {
        $data = $request->all();
        $inp['end_hour'] = now();
        $this->billRepo->update($inp, $id);
        $input = array();
        $input['bill_id'] = $data['bill_id'];
        $bill_id = $data['bill_id'];
        $sum = 0;
        foreach ($data as $key => $val) {
            if ($key != 'bill_id' && $key != '_token') {
                $stock = $this->stockproductRepo->whereare($key, 1);
                if ($stock) {
                    $product = $this->detailRepo->getwhere($bill_id, $key);
                    $inputs['number'] = $stock->number - ($val - $product->number);
                    $this->stockproductRepo->update($inputs, $stock->id);
                }
                $value = $this->detailRepo->getwhere($bill_id, $key);
                $input['number'] = $val;
                $this->detailRepo->update($input, $value->id);
                $product = $this->productRepo->find($key);
                $sum = $sum + ($product->input_price * $val);
            }
        }
        $bill = $this->billRepo->find($bill_id);
        $total = ($sum + (((strtotime(date($bill->end_hour)) - strtotime(date($bill->start_hour))) / 3600) * $bill->room->price));
        $datas['total'] = $total;
        $this->billRepo->update($datas, $id);
        return redirect()->route('admin.index')->with('success', trans('base.book_room_success'));
    }

    public function confirm(Request $request) {
        $id = $request->get('id');
        $bill = $this->billRepo->confirmInvoice($id);
        $bills = $this->billRepo->find($id);
//        if ($bills->room->position == 1) {
//            $input['stock_id'] = 2;
//        } else {
//            $input['stock_id'] = 3;
//        }]]
        $inp['sale'] = $request->get('sale');
        $inp['sale_bill'] = $request->get('sale_bill');
        $inp['total'] = $request->get('total');
        $inp['surcharge'] = $request->get('surcharge');
        $this->billRepo->update($inp, $id);
        return response()->json(['sucess' => true]);
    }

    public function invoice($id) {
        $bill = $this->billRepo->find($id);
        $sum = 0;
        if ($bill->status == 1) {
            foreach ($bill->billdetail as $value) {
                $sum = $sum + ($value->product->price * $value->number);
            }
            $total = $sum + (round(((strtotime(date($bill->end_hour)) - strtotime(date($bill->start_hour))) / 3600), 1) * $bill->room->price);
            $bill->total = $total;
            $data['total'] = $total;

            $this->billRepo->update($data, $id);
        }
        $product = $this->productRepo->all();
        return view('backend/bill/invoice', compact('bill', 'product', 'id'));
    }

    public function in() {
        $billpros = $this->billproRepo->getAll();
        return view('backend/bill/incom', compact('billpros'));
    }

    public function incom(Request $request) {
        $start = $request->get('start');
        $end = $request->get('end');
        $bills = $this->billRepo->time($start, $end);
        $stocks = $this->billproRepo->time($start, $end);
        $sum = 0;
        $sums = 0;
        foreach ($bills as $value) {
            $sum = $sum + $value->total;
        }
        foreach ($stocks as $value) {
            $sums = $sums + $value->total;
        }
        $billpros = $this->billproRepo->getAll();
        return view('backend/bill/incom', compact('end', 'start', 'sum', 'sums', 'billpros'));
    }

    public function cancel($id) {
        $stt = $this->billRepo->cancel($id);
        return json_encode([
            'success' => $stt
        ]);
    }

    public function createin() {
        return view('backend/bill/bill_create');
    }

    public function create(Request $request) {
        $input = $request->all();
        $code = $this->billproRepo->getMaxValue();
        if (is_null($code)) {
            $code = 'PC' . date("dmy") . '-00000';
        }
        $input['code'] = ++$code;
        $input['user_id'] = \Auth::user()->id;
        $input['total'] = str_replace(',', '', $request->get('total'));
        $this->billproRepo->create($input);
        $billpros = $this->billproRepo->getAll();
        return redirect()->route('admin.bill.in', ['billpros' => $billpros])->with('success', trans('Thêm thành công'));
    }

    public function bill_detail($id) {
        $billpro = $this->billproRepo->find($id);
        return view('backend/bill/bill_detail', compact('billpro'));
    }

}
